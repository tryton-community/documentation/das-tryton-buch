# Tryton Administrator-Handbuch
Dieses Handbuch hat seinen Schwerpunkt bei der Installation von Tryton in einer *virtuellen Umgebung für python* (VENV) mit pip.	

## Installation auf (ubuntu-)Linux
Normalerweise ist die Eingabe dieser Befehle nicht notwendig, weil unser Installationsskript sie automatisch abarbeitet.

### Datenbank erzeugen
PostgreSQL installieren und starten:

`$ sudo apt install postgresql`

Dieser Befehl stellt sicher, daß der Datenbank-Server bei jedem Systemstart gestartet wird:

`$ sudo systemctl start postgresql`

Als Benutzer "postgres" anmelden (Name des Verwalters des Datenbank-Servers):

`$ sudo su - postgres`

```
(postgres)$ psql
=# CREATE ROLE [Name-gewünschter-Datenbank-Benutzer] WITH PASSWORD [Ihr-Kennwort];
=# \q
(postgres) $ createdb -O [Name-des-Datenbank-Benutzers] [gewünschter-Name-der-neuen-Datenbank]
exit
```

Damit haben sie eine Datenbank mit dem Namen [Name-der-neuen-Datenbank], die dem Benutzer [Name-des-Datenbank-Benutzers] gehört. Notieren Sie Namen der Datenbank, des Benutzers und das Kennwort.

### Tryton für PIP installieren{#trytonfuerpipinstallieren}

Sollten Sie Tryton für PIP vorher systemweit installiert haben, müssen Sie diese Teile deinstallieren. Sehen Sie in `/usr/local/lib/python3.8/dist-packages/` nach und entfernen Sie alle Tryton Pakete mit dem Befehl:

`$ pip uninstall trytond_**`

(`**` steht dabei für die Namen der jeweiligen Komponenten)

Installieren Sie python3-pip und python3-venv:

`$ sudo apt install python3-pip python3-venv`

*Eine neue virtuelle Umgebung erzeugen:*

Falls Sie mehrere Tryton-Versionen verwenden wollen, ist es sinnvoll, im Heimatverzeichnis ein Verzeichnis für alle virtuellen Tryton-Umgebungen zu erstellen:

`$ mkdir ~/TRYTON`

```
$ cd [~/TRYTON]
$ python3 -m venv --system-site-packages [Name-der-neuen-Umgebung] # zum Beispiel "try-60-prod" für "Version 6.0 produktiv"
```

Wechseln Sie in die Umgebung und aktivieren Sie sie:

```
$ cd [Name-der-virtuellen-Umgebung]
$ . bin/activate
```

PIP updaten:

`$ pip install --upgrade pip`

*Tryton installieren.*

Zur Installation einer bestimmten Version, verwenden Sie zwei Gleichheitszeichen:

`$ pip install trytond==5.4.*` installiert die neueste Trytond-Version aus der Serie 5.4.

Um die neueste Version zu installieren, verwenden Sie:

`$ pip install trytond`

Installieren Sie psycopg2-binary:

`$ pip install psycopg2-binary`

Erzeugen Sie in Ihrer virtuellen Umgebung eine Datei "tryton.conf" mit folgendem Inhalt:

```
[database]
uri = postgresql://[Name-des-Datenbank-Benutzers]:[database-password]@localhost:5432/
language = de
```

(Sie haben weiter oben die Daten aufgeschrieben - korrekt??)

Installieren Sie die Liste der Module, die Sie verwenden möchten:

`pip install -r [list-of-modules.txt]`

Einzelne Module können so installiert werden:

`$ pip install trytond_##` "##" bedeutet den Namen des gewünschten Moduls.

Stellen Sie sicher, daß Sie in Ihrer virtuellen Umgebung sind und diese aktiviert ist.

Dann:

`(Ihre-virtuelle-Umgebung) $ trytond-admin -c trytond.conf -d [Name-der-neuen-Datenbank] --all -vv`

Die Datenbank wird nun mit den grundlegenden Tabellen und Daten befüllt.

Starten Sie den Tryton-Dämon mit:

`(Ihre-virtuelle-Umgebung) $ trytond -c trytond.conf -d [Name-der-neuen-Datenbank]`

Sie müßten sich jetzt mittels des Tryton-Klienten mit der Datenbank verbinden können. Dort verwenden Sie [localhost:8000] als hostname und [Name-der-neuen-Datenbank] als Datenbank.

## Installation Server und Client auf MS Windows{#installationserverundclientaufmswindows}

Installieren Sie

* postgresql von https://www.postgresql.org/download/windows/
* python von https://www.python.org/downloads/

Rufen Sie PgAdmin auf und erzeugen Sie eine Benutzerrolle mit Kennwort und eine Datenbank. Tragen Sie gegebenenfalls die passende Umgebungsvariable für python ein.

Im Terminal:

`$ python -m venv --system-site-packages [Name-der-neuen-Umgebung] # zum Beispiel "try-60-prod" für "Version 6.0 produktiv"`

Wechseln Sie in die Umgebung und aktivieren Sie sie:
```
cd [Name-der-virtuellen-Umgebung]/Scripts
activate
```

Updaten Sie pip:

`$ python pip install --upgrade pip`

Zur Installation einer bestimmten Version, verwenden Sie zwei Gleichheitszeichen:

`$ python pip install trytond==5.4.* # installiert die neueste Trytond-Version aus der Serie 5.4.`

Um die neueste Version zu installieren, verwenden Sie:

`$ python pip install trytond`

Installieren Sie psycopg2-binary:

`$ python pip install psycopg2-binary`

Erzeugen Sie in Ihrer virtuellen Umgebung im Verzeichnis "Scripts" eine Datei “trytond.conf” mit folgendem Inhalt:

```
[database]
uri = postgresql://[Name-des-Datenbank-Benutzers]:[database-password]@127.0.0.1:5432/
language = de
```
(Sie haben weiter oben die Daten aufgeschrieben – korrekt??)

Installieren Sie die Liste der Module, die Sie verwenden möchten:

`$ python pip install -r [Liste-der-Module.txt]`

Einzelne Module können so installiert werden:

`$ python pip install trytond_%%%  #  “%%%” bedeutet den Namen des gewünschten Moduls`

Stellen Sie sicher, daß Sie in Ihrer virtuellen Umgebung sind und diese aktiviert ist. Registrieren Sie die Datenbank in Ihrem VENV, die Datenbank wird nun mit den grundlegenden Tabellen und Daten befüllt:

`$ python Scripts\trytond-admin -c trytond.conf -d [Ihre-Datenbank] --all -vv`

Starten Sie den Tryton-Dämon mit:

`$ (Ihre-virtuelle-Umgebung)/Scripts $ trytond -c trytond.conf -d [Name-der-neuen-Datenbank]`

Im Tryton-Klient können Sie sich nun mit der Datenbank verbinden. Dort verwenden Sie als hostname 127.0.0.1 (die Verwendung von "localhost" kann zu SEHR langsamem Arbeiten des Systems führen)und [Name-der-neuen-Datenbank] als Datenbank.

## Pflege des Systems

### Datenbank sichern und zurückspielen

Regelmäßige Sicherungen der Datenbank sind Pflicht. In der Datenbank ist praktisch alles gespeichert: Ihre Tryton-Einrichtung, Ihre Kunden, Buchungen etc. Insofern lohnt sich ein sorgsamer Umgang damit.

#### Datenbank sichern

Stellen Sie sicher, daß PostgreSQL läuft.

Dann:

```
$ killall trytond
$ killall trytond-cron
$ pg_dump -U [Name-des-Datenbank-Benutzers] -d [Name-der-Datenbank]> [gewünschter-Name-der-Sicherungskopie].sql
```

#### Sicherungskopie zurückspielen

Tryton Client und -Dämon beenden.

```
$ cd [Ihre-virtuelle-Umgebung]
$ deactivate
$ killall trytond
```

Werden Sie der Datenbankbenutzer "postgres":

`sudo su - postgres`

Löschen Sie die alte Datenbank (optional) und erzeugen eine neue:

```
(postgres) $ dropdb [Name-der-vorherigen-Datenbank]
(postgres) $ createdb -O [Name-des-Datenbank-Benutzers] [Name-der-neuen-Datenbank]
exit
```

Als normaler Benutzer: Aktivieren Sie Ihre virtuelle Umgebung und registrieren Sie die neue Datenbank in Ihrer virtuellen Umgebung:

```
$ psql -d [Name-der-neuen-Datenbank] -U [Benutzername] < [Name-Ihrer-Sicherungsdatei].sql
$ cd [Ihre-virtuelle-Umgebung]
$ . bin/activate
(Ihre-virtuelle-Umgebung) $ trytond-admin -c [Ihre-trytond.conf] -d [Name-der-neuen-Datenbank] --all -v
```

## Grundlegende Handhabung von Tryton mit PIP

### Ein Modul installieren

Gegebenenfalls den Tryton-Klienten und -Dämon stoppen und Ihre virtuelle Umgebung aktivieren:

```
$ killall trytond
$ killall trytond-cron
$ cd [Ihre-virtuelle-Umgebung]
$ . bin/activate
```

Modul installieren:

`(Ihre-virtuelle-Umgebung) $ pip install trytond-[name-of-your-module]`

Modul in der Datenbank registrieren:

`(Ihre-virtuelle-Umgebung) $ trytond-admin -c [Ihre-trytond.conf] -d [Name-Ihrer-Datenbank] --all -v`

Trytond neustarten:

`(Ihre-virtuelle-Umgebung) $ trytond -c [Ihre-trytond.conf] -d [Name-Ihrer-Datenbank] -v`

Im Tryton-Klient:

![](../wp-content/uploads/icons/tryton-menu.svg) Verwaltung > Module > Module

"aktivieren" doppelklicken

Aktivierung durchführen (Propeller-Symbol)

#### Postgres starten

Diese Aktion ist nur einmal notwendig, danach startet postgres bei jedem Systemstart automatisch.

`$ sudo systemctl start postgresql`

#### Ihre virtuelle Umgebung aktivieren

```
$ cd ~/[Name-der-Umgebung]
$ . bin/activate
```

#### Den Tryton-Dämon starten

Mit aktivierter virtueller Umgebung:

`(Ihre-virtuelle-Umgebung) $ trytond -c trytond.conf -d [Name-Ihrer-Datenbank]`

#### Den Tryton-Klienten starten

Sofern nicht systemweit installiert und über das Menü aufrufbar:

`$ [Pfad-zum]/tryton-[version]/bin/.tryton`

### PIP Installation updaten

Grundsätzlich ist es möglich, mehrere Versionsnummern zu überspringen. Wer sich aber in der Struktur der Datenbank nicht gut auskennt, sollte Schritt für Schritt vorgehen - also von 6.0 nach 6.2 nach 6.4 etc. 

Wenn Sie unnötige Module deinstallieren wollen, ist dies die Gelegenheit. Deaktivieren Sie sie unteren

![](../wp-content/uploads/icons/tryton-menu.svg) Verwaltung > Module > Module

#### Datenbank sichern

Normalerweise sollte das von unserem Sicherungsskript automatisch erledigt werden. Ansonsten:

```
$ cd [Ihre-virtuelle-Umgebung]
$ pg_dump -U [Name-des-Datenbankbenutzers] d- [Name-der-aktuellen-Datenbank] > [Name-Sicherungsdatei].sql
```

#### Sichern sie eine Liste der installierten Pakete

```
. bin/activate
$ pip freeze --local > [Liste-Module-heute-installiert.txt]
```
Entfernen Sie die vorher deaktivierten Module aus dieser Liste sowie alle anderen Pakete mit Ausnahme derer, die mit "trytond" beginnen.

#### Neue virtuelle Umgebung

Erzeugen Sie eine neue virtuelle Umgebung und aktivieren Sie sie:

```
$ python3 -m venv --system-site-packages [Pfad-zum-Verzeichnis-neue-virtuelle-Umgebung]
$ cd [Pfad-zum-Verzeichnis-neue-virtuelle-Umgebung]
$ . bin/activate
```

Kopieren Sie die *.sql-Sicherungsdatei sowie die Modulliste in das Verzeichnis der neuen virtuellen Umgebung, ebenso externe Module (*.whl-Dateien). Ersetzen Sie in der Modulliste die Versionsnummern durch die Zielversionsnummern, zum Beispiel: 

```
[Name-Tryton-Modul]==5.4.19
durch
[Name-Tryton-Modul]==5.6.*
```

Der "*" bedeutet, daß die aktuellste Version aus der Serie 5.6 installiert wird. 

Upgraden Sie pip und installieren Sie die Module der Liste:

```
$ pip install --upgrade pip
$ pip install --upgrade -r [Installs-[of-today].txt]
```

#### Neue Datenbank

Erzeugen Sie eine neue Datenbank in einem neuen Terminal und importieren Sie die sql-Sicherungsdateil

```
$ sudo su - postgres
(postgres) $  createdb -O [Name-des-Datenbankbenutzers] [Name-der-neuen-Datenbank]
(postgres) $  exit
$ psql -d [Name-der-neuen-Datenbank] -U [Name-des-Datenbankbenutzers] < [Pfad&Name-der-sql-Sicherungsdatei]
```

#### Vorbereitungen zum Update

Anweisungen für die beim jeweiligen Update-Schritt manuell durchzuführenden Eingaben sind hier aufgelistet: <https://discuss.tryton.org/c/migration/6>.

In der Regel handelt es sich dabei um SQL-Eingaben, dabei ist darauf zu achten, WANN sie durchgeführt werden müssen. Hier ein Beispiel für ein Upgrade von 5.8 nach 6.0: <https://discuss.tryton.org/t/migration-from-5-8-to-6-0/3603:>

1. Vor dem Upgrade: Eine neue Spalte einfügen 
2. das Upgrade durchführen
3. nach dem Upgrade einige Einstellungen der Rechnungstabelle korrigieren und eine unbenutzte Spalte entfernen - **Beispiel**:

```
$ psql -d [Name-der-neuen-Datenbank] -U [Name-des-Datenbankbenutzers]
[Name-der-Datenbank]=> ALTER TABLE ir_model_field ADD COLUMN "access" BOOLEAN;
[Name-der-Datenbank]=> [weitere SQL-Befehle]
[Name-der-Datenbank]=> \q
```

Ihre virtuelle Umgebung muß aktiv sein, dann die neue Datenbank in Tryton registrieren:

```
$ trytond-admin -c trytond.conf -d [Name-der-neuen-Datenbank] --all -v
```

Dieser Schritt kann einige Zeit in Anspruch nehmen. Die Fehlermeldung "module xyz not found läßt sich meist durch Nachinstallation des Moduls via pip beheben; danach muß der obenstehende `trytond-admin`-Befehl wiederholt werden.

Wenn das Update ohne Fehler durchgelaufen ist, muß die Datenbank "aufgeräumt" werden:

```
$ psql -d [Name-der-neuen-Datenbank] -U [Name-des-Datenbankbenutzers]
[Name-der-Datenbank]=> UPDATE account_invoice_line SET currency = (SELECT currency FROM account_invoice WHERE id = account_invoice_line.invoice);
[Name-der-Datenbank]=> UPDATE account_invoice_line SET invoice_type = (SELECT type FROM account_invoice WHERE id = account_invoice_line.invoice) WHERE invoice_type IS NOT NULL;
[Name-der-Datenbank]=> UPDATE account_invoice_line SET party = (SELECT party FROM account_invoice WHERE id = account_invoice_line.invoice) WHERE party IS NOT NULL;
[Name-der-Datenbank]=> ALTER table ir_sequence_type DROP COLUMN code;
[Name-der-Datenbank]=> \q
```
Danach muß der obenstehende `trytond-admin`-Befehl wiederholt werden.


## Tryton für die Nutzung mit Web-Browser einrichten

Tryton kann anstatt mit dem Desktop-Client auch mit einem Web-Browser benutzt werden. Diese Variante bietet einfachen Zugriff auf Tryton ohne weitere Installationen, aber etwas weniger Bedienkomfort.

Für die browserfähige Auszulieferung von Tryton-Webseiten existiert eine nodejs-Webapplikation names "sao", deren Installation und Einrichtung hier beschrieben wird.

### Nodejs installieren

Installation von *nodejs* bzw. *npm* (Node Package Manager) über die Paketverwaltung der jeweiligen Linux-Distribution. Unter Debian / Ubuntu:

`$ sudo apt-get install npm`

### Das Paket sao installieren und einrichten
Das Paket sao enthält die nodejs-Webapplikation für Tryton und ist unter downloads.tryton.org zu finden. Wichtig ist es, die zur trytond-Version passende Version von sao zu benutzen, also zum Beispiel bei trytond 6.0.x die Version tryton-sao-6.0.x:
  ```
$ cd [pfad-zum-VENV]
$ wget https://downloads.tryton.org/6.0/tryton-sao-latest.tgz
$ tar -xzf tryton-sao-latest.tgz
$ mv package sao
$ cd sao
$ npm install --production --legacy-peer-deps
 ```
 
 Zum Schluss wird noch die Konfigurationsdatei von Tryton `trytond.conf` um ein paar Einträge erweitert:
 
```
[web]
listen=0.0.0.0:8000 # gegebenenfalls Port-Nummer anpassen
root = [Pfad-zum-VENV]/sao
```
Wenn man jetzt `trytond` wie gewohnt startet, kann man Tryton mit dem Web-Browser unter der URL [http://localhost:8000](http://localhost:8000) benutzen; auf einem Server lautet der Aufruf `[URL-des-Servers:Portnummer]`.

## UWSGI-Server
Beim Start des `trytond` auf der Konsole erscheint die Warnmeldung

> WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead

Dies kann bei kleineren Installationen in der Regel ignoriert werden. Wer aber zum Beispiel mehrere Instanzen auf einem Server betreibt, kann die Performance mit einem UWSGI-Server steigern. UWSGI ist WSGI-Implementierung und ist in der Programmiersprache 'C' geschrieben, dadurch ist diese leistungsfähiger als der WSGI-Standard-Server namens "Werkzeug", der in Python programmiert ist.

Zu installieren sind:

$ `sudo apt install uwsgi uwsgi-plugin-python3`

UWSGI benötigt eine *Konfigurationsdatei*. Diese kann irgendwo im System placiert werden, naheliegend ist das VENV-Verzeichnis.
```
[uwsgi]
http-socket=0.0.0.0:8000 # ggf. Port-Nummer anpassen
master=true
plugins=python3
wsgi=trytond.application:app
cheaper=4
processes=16
threads=16
virtualenv=[Pfad-zum-VENV] # absoluten Dateipfad zum VENV
```
Es müssen weiterhin mehrere *Umgebungsvariablen* gesetzt sein, die Angaben zur Datenbankverbindung und Verweise zu Konfigurationsdateien enthalten:
```
TRYTOND_DATABASE_URI="postgresql://db_username:[db_password]@[db_hostname]:[db_port]"
TRYTOND_LOGGING_CONFIG="[Pfad-zu]/trytond_log.conf"
TRYTOND_CONFIG="[Pfad-zu]/trytond.conf"
PYTHONOPTIMIZE="1"
```

`TRYTOND_LOGGING_CONFIG`ist so lange nicht erforderlich, so lange man in der Tryton-Konfiguration keine Angaben zu Protokolldateien (Logging) macht.

`TRYTOND_DATABASE_URI`enthält die Angaben zur Datenbankverbindung und ist identisch mit dem Eintrag in der Konfigurationsdatei `trytond.conf`.

Der Pfad im Eintrag `TRYTOND_CONFIG` muss auf den absoluten Dateipfad zur Trytond-Konfigurationsdatei angepasst werden.

Die Umgebungsvariablen müssen sowohl beim Aufruf von UWSGI als auch bei Aufruf des Klienten gesetzt sein. Dies kann man ggf. in ein bestehendes Startskript aufnehmen oder ein eigenes Shellskript anlegen. Die Umgebungsvariablen müssen in der Form

`export TRYTOND_DATABASE_URI="..."`

usw. angelegt werden.

Der *Aufruf* von uwsgi erfolgt mit dem Befehl

`uwsgi --ini [Pfad-zu]/uwsgi.conf`

Die Pfadangabe muss auf den absoluten Dateipfad der UWSGI-Konfigurationsdatei angepasst werden.


