# Mögliche Funktionserweiterungen - weitere Module
Wir listen hier nur die oft nachgefragten Funktionen auf, die durch weitere Module ergänzt werden können. Diese Aufzählung ist nicht erschöpfend, Tryton kann  darüberhinaus ungleich mehr. Einige Details finde sich unter [https://docs.tryton.org/en/latest/#modules](https://docs.tryton.org/en/latest/#modules). Wir empfehlen außerdem eine Anfrage im [Tryton-Forum](https://discuss.tryton.org).

## Versandwesen
### Versandkosten{#versandkosten}
**Erforderliche Module:** *trytond-sale-shipment-cost und trytond-stock-shipment-cost*

Sofern die Versandart nach Gewicht oder nach "Prozentsatz" (leider schweigt sich die offizielle Tryton-Dokumentation darüber aus, was das bedeuten soll) automatisiert ausgewählt werden soll:  
*trytond-carrier-weight*, *trytond-stock-shipment-cost-weight* oder *trytond-carrier-percentage*. Wir freuen uns über Hinweise, wie diese Module zusammenspielen und was sie bewirken.

*Hinweis:* In den Versionen 6.6, 6.8 und 7.0 existiert u.E. ein Benennungsfehler: Unter  
![](../wp-content/uploads/icons/tryton-menu.svg) > Versand > Einstellungen > Auswahl, außerdem beim Verkauf und dem Warenausgang sind Felder mit "Versanddienstleister" bezeichnet, die aber "Versanddienstleistung" heißen müßten.

#### Vorbereitung
+ Erstellen Sie Parteien Ihrer Versanddienstleister, also Spediteure, Paketdienste etc.
+ Stellen Sie sicher, dass in den Parteien Ihrer eigenen Firma, Ihrem Lager (?) und den betreffenden Kunden das Feld "Land" gesetzt ist. Falls Sie das nicht bei allen Kunden tun möchten, lassen Sie bitte unter ![](../wp-content/uploads/icons/tryton-menu.svg) > Versand > Einstellungen > Auswahl das Empfängerland leer.
+ In ![](../wp-content/uploads/icons/tryton-menu.svg) > Artikel müssen die Versanddienstleistungen als Artikel vorhanden sein bzw. angelegt werden. Sie müssen den Typ "Service" aufweisen, als "verkäuflich" markiert sein und es muß eine Buchhaltungskategorie eingetragen sein. Jede Versand- und Zustellart muß als separater Artikel angelegt werden, also zum Beispiel "Fa. ABC Standardversand" und "Fa. ABC Expreßversand".
+ In ![](../wp-content/uploads/icons/tryton-menu.svg) > Versand > Versanddienstleister werden die vorher erstellen "Versandartikel" in Versanddienstleistungen zu verwandeln, indem man die "Versandprodukte" den Versanddienstleistern zuordnet. 
+ In ![](../wp-content/uploads/icons/tryton-menu.svg) > Versand > Einstellungen > Auswahl können Sie einschänken, welche Versanddienstleistungen im Verkauf zu Auswahl stehen - abhängig vom Ursprungs- und Bestimmungsland. Es macht also nichts, wenn Sie die Felder "von Land" und "nach Land" nicht ausfüllen - Sie erhalten einfach eine sehr lange Liste im Verkauf. (Hinweis siehe unten)
+ In ![](../wp-content/uploads/icons/tryton-menu.svg) > Versand > Versanddienstleister > (jeweilige Position öffnen) können Sie Vorgabewerte für die *Versandkostenmethode* auswählen.
+ Die voreingestellte Versandart kann im Verkauf und im Warenausgang bei Bedarf überschrieben werden.

#### Handhabung
Wenn Sie nun einen Verkauf anlegen, finden Sie das Feld "Versanddienstleister" (das u.E. "Versanddienstleistung" heißen sollte), hier sollte der Vorgabewert für die Versanddienstleistung stehen. 

Unter ![](../wp-content/uploads/icons/tryton-menu.svg) > Lager&Inventur > Lieferungen > Warenausgang können Sie die Versanddienstleistung vor Abschluss der Sendung ändern. Diese wird dann in die Kundenrechnung eingefügt.

### Abmessungen und Gewichte zu Artikeln hinzufügen

**Erforderliche Module:** *trytond-product-measurements* und *trytond-stock-shipment-measurements*

Diese Module ergänzen den Bereich "Artikel" um einen Reiter "Abmessungen", in dem Länge, Breite, Höhe, Volumen und Gewicht eingetragen werden können.

Um diese Angaben in einem Bericht (Kommissionierliste, Lieferschein etc.) verwenden zu können, kann dort 

* zum Beispiel für das Gewicht pro Zeile, also pro Sendungsposition eine zusätzliche Spalte angelegt werden mit dem Eintrag: `<(format_number(move.product.weight*move.quantity, user.language, digits=move.product.weight_uom.digits) + ' ' + move.product.weight_uom.symbol) if (move.product.weight and move.quantity) else 'n.e'>`.  
Dieser fängt zusätzlich leere Einträge beim Artikel ab und trägt dort "n.e." (nicht erfaßt) ab. Analog sollte das für "length", "volume" etc. funktionieren.
* Das gesamte (Netto)Gewicht einer Sendung kann durch den Eintrag:  
<format_number(shipment.weight, user.language, digits = 1)>  <shipment.weight_uom.symbol>  
in eine neue Zeile des Berichts, zwischen den beiden `</FOR>`-Anweisungen, erzeugt werden.

Unsere Muster-Vorlage unter [https://foss.heptapod.net/tryton-community/tools/berichtsvorlagen-deutschland]() enthält diese Einträge bereits.

### Sendungen zusammenfassen
*Benötigtes Modul: trytond-sale-shipment-grouping*

#### Einstellungen
![](../wp-content/uploads/icons/tryton-menu.svg) > Verkauf > Einstellungen > Gruppierungsmethode Lieferungen Verkauf  
auf "Standard".

#### Handhabung
Um aus mehreren wartenden Sendungen eine gemeinsame Kommissionierliste zu erzeugen, muß man

1. prüfen, ob wartende Sendungen im System vorhanden sind
2. "geplantes Datum" auf das heutige Datum setzen
3. einen neuen Verkauf und daraus eine neue Lieferung erstellen
4. die erstellte Lieferung enthält dann alle bis dahin aufgelaufenen Bestellungen und kann ausgeliefert werden.

Die Schritte 1 und 2 können wie folgt automatisiert werden:

Unter
![](../wp-content/uploads/icons/tryton-menu.svg) > Verwaltung > Planer > Aktionen

wählen Sie 'Kundensendungen neu einplanen' und stellen Sie das gewünschte Intervall ein. Dann werden alle wartenden Sendungen für den heutigen Tag neu eingeplant und können mit 'heute' neu hinzukommenden Sendungen zusammengefaßt werden.

### Direktlieferung
*erforderliches Modul: `trytond-sale-supply-drop-shipment`*

#### Beschränkungen:
+ Der Versand ist nur von einem Verkäufer zu einem Käufer möglich, Sammelbestellungen zum Versand an mehrere Käufer oder von mehreren Lieferanten müssen mit mehreren Vorgängen abgewickelt werden.
+ Die Sendung darf unterschiedliche Artikel enthalten.
+ Ein Artikel muß als "Artikel für Direktlieferung" markiert werden und kann dann nicht aus dem eigenen Lager ausgeliefert werden. Man kann sich aber damit behelfen, daß man das selbe Produkt als zwei Artikel anlegt, eines zur Lieferung aus eigenem Lager, eines für die Direktlieferung.

#### Vorbereitungen
+ Nummernkreis "Direktlieferung" einrichten - vermutlich kann man diese Bezeichnung nicht ändern
+ ![](../wp-content/uploads/icons/tryton-menu.svg) > Lager&Inventur > Einstellungen > Einstellungen: "Nummernkreis Direktlieferung" auf "Direktlieferung" setzen
+ ![](../wp-content/uploads/icons/tryton-menu.svg) > Artikel, Reiter "Lieferanten": "Beschaffung bei Verkauf" auf "immer" oder "Lagerbestand zuerst" setzen. Letztere Option verkauft erst den Lagerbestand und erzeugt eine Direktlieferung erst dann, wenn dieser nicht ausreicht. 
+ ebenda: Einen Lieferanten auswählen, dort "Direktlieferung" auswählen und die Beschaffungszeit auf "00:00" setzen.
+ einen Versand-Artikel erzeugen, "käuflich" und "verkäuflich" auswählen;  unter dem Reiter "Lieferanten" bei "Beschaffung bei Verkauf" "immer" auswählen. Evt. ist ein Neustart von Tryton nötig, um die letzte Änderung anzuwenden.

#### Handhabung
+ Verkauf erzeugen und bestätigen
+ Unter ![](../wp-content/uploads/icons/tryton-menu.svg) > Einkauf > Bestellvorschläge wurde ein Bestellvorschlag für die Direktlieferung erzeugt. Diesen markieren und mit ![](../wp-content/uploads/icons/tryton-launch.svg) "Einkauf erstellen" bestätigen.
+ Unter ![](../wp-content/uploads/icons/tryton-menu.svg) > Einkauf > Einkauf den betreffenden Einkaufsentwurf öffnen und bestätigen. Es wird dann eine Lieferantenrechnung erzeugt, die noch festgeschrieben werden muß.
+ Im Einkauf unten ist jetzt eine Direktlieferung (1) zu sehen - diese anklicken
+ "versenden" und "erledigt" klicken um die Lieferung abzuschließen
+ Die erzeugten Kunden- und Lieferantenrechnungen müssen noch abschließend bearbeitet werden (festgeschrieben) werden.


## Preislisten
*erforderliches Modul: sale_price_list*

![](../wp-content/uploads/icons/tryton-menu.svg) > Artikel > Preislisten

Preislisten werden in Tryton sowohl zur Erzeugung von Kundenpreislisten (zum Beispiel in Katalogen) als auch für die Abbildung von unterschiedlichen Verkaufspreisen für unterschiedliche Kunden(gruppen) in Kundenrechnungen etc. verwendet. Ein klassischer Anwendungsfall sind Preislisten für "Endkunden" und "Wiederverkäufer". Die Funktion ist sehr mächtig, dadurch aber auch komplex in der Anwendung. Für anspruchsvollere Situationen wirken "Preislisten" und "Artikelkategorien" zusammen. 

### Anwendung ohne Artikelkategorien
Unter ![](../wp-content/uploads/icons/tryton-menu.svg)  > Artikel > Preislisten wird mit ![](../wp-content/uploads/icons/tryton-create.svg) eine neue Preisliste angelegt, in der Liste "Positionen" können mit dem kleinen ![](../wp-content/uploads/icons/tryton-add.svg)   Einträge hinzugefügt werden. Ein solcher ist 

+ entweder ein Artikel 
+ oder eine Artikelkategorie (s. weiter unten)

Der Eintrag im Feld "Formel" enthält 

+ entweder eine Zahl (den VK-Preis) 
+ oder eine "echte Formel", hier können Berechnungen mit dem Listenpreis (list_price), dem Einstandspreis (cost_price) oder dem unit_price mit Python-Code durchgeführt werden.  
 *Achtung, hier stets den Dezimalpunkt statt eines Kommas verwenden*.

Beispiele:

Eintrag | Resultat 
:--- | :--- 
4| Artikelpreis ist 4€  
`cost_price` * 1.5 | Einstandspreis +50%
`list_price` * 0.9 | Listenpreis -10%

In einer Preisliste können Positionen, die Artikel enthalten, mit solchen, die Artikelkategorien enthalten, kombiniert werden. Wichtig ist, **daß diese Einträge von oben nach unten abgearbeitet werden**. Tryton wendet stets den ersten gefundenen Eintrag an, daher ist die Reihenfolge der Positionen entscheidend.

Bei Problemen mit der Preisliste kann es helfen, einen letzten Eintrag ohne Kategorie und Artikel, mit Menge 1 und Formel `unit_price` anzulegen.

### Anwendung mit Artikelkategorien
Diese werden unter ![](../wp-content/uploads/icons/tryton-menu.svg) > Artikel > Kategorien angelegt. In unserem Fall dienen sie dazu, Gruppen von Artikeln zu bilden, auf die die gleiche Kalkulationsregel (oder der gleiche Zahlenwert) angewendet wird. Mit ![](../wp-content/uploads/icons/tryton-add.svg) Artikel hinzufügen kann entweder in dem Eingabefeld ein einzelner Artikel eingegeben, oder mehrere Artikel mit ![](../wp-content/uploads/icons/tryton-add.svg)  ausgewählt werden.

Um für einzelne Artikel Ausnahmen für eine Artikelkategorie festgelegt werden, erzeugt man für diese Artikel Einzeleinträge (oder eine weitere Kategorie), die in der Preisliste **oberhalb** der ersten Artikelkategorie eingeordnet werden.

https://www.tryton-dach.org/tryton-buch-details/

## Anlagebuchhaltung - Abschreibungen
*erforderliches Modul: trytond_account_assets*

### Prämissen
Das Modul bildet die französische Buchungsweise ab und ist daher für deutsche Benutzer etwas ungewohnt; dennoch ist es in Deutschland nutzbar.  

Bei nachträglicher Installation des assets-Moduls muß man den Assistenten "Kontenplan von Vorlage aktualisieren" ausführen.

#### Abschreibungskonten überprüfen
Vorab ist zu überprüfen, ob alle Konten unter  
![](../wp-content/uploads/icons/tryton-menu.svg) > Aktiva > Anlagevermögen > Immaterielle Vermögensgegenstände, Sachanlagen und Finanzanlagen (mit allen Untereinträgen)  

wie folgt als Abschreibungskonten eingerichtet sind:  
Unter ![](../wp-content/uploads/icons/tryton-menu.svg) > Rechnungswesen > Einstellungen > Konten > Kontentypen: 
+ Vorlage überschreiben
+ Aktiva
+ Anlagevermögen 

#### Rechnungswesen > Einstellungen
+ Nummernkreis Anlage einrichten und auswählen
+ prüfen, ob Standard-Aufwands- und Ertragskonto angelegt sind
+ Abschreibungsbuchungsfrequenz einstellen (monatlich/jährlich) 

#### Artikel > Kategorie
Pro Anlageklasse (EDV-Software, Maschinen, PKW etc.) wird eine Artikelkategorie angelegt. Dort (Reiter Buchhaltung) wird eingetragen:

+ Ertragskonto: im SKR04 ab 4910, passend zur Anlageklasse
+ Aufwandskonto: im SKR04 ab 6200, passend zur Anlageklasse
+ Abschreibungskonto: im SKR04 ab 0100, passend zur Anlageklasse
+ Anlagenkonto: wie Abschreibungskonto

#### Artikel
Sofern der Artikel bereits vorhanden ist, müssen die Einstellungen wie unten dargestellt geändert werden; ansonsten wird ein neuer Artikel angelegt.

+ Typ: Anlagegut  

Reiter Buchhaltung:
+ vorher erstellte Buchhaltungskategorie auswählen
+ abschreibungsfähig
+ Abschreibungsdauer in Monaten (abhängig von Abschreibungsbuchungsfrequenz prüfen???)
+ Wertangabe ist irrelevant für Abschreibungen

### Neue Anlage eintragen
Unter ![](../wp-content/uploads/icons/tryton-menu.svg) Rechnungswesen > Anlagenverwaltung > Anlagen:
+ Wert: dieser ist der relevante Eintrag, der im Artikel wird nicht berücksichtigt
+ Restwert: normalerweise 1€
+ Abschreibungen Vortrag: Normalerweise 0, außer, es wurden bereits Abschreibungen vorgenommen - typischerweise vor Einführung von Tryton.

### Abschreibungsbuchungssätze erzeugen
Assistent "Abschreibungsbuchungssätze (Positionen) erstellen" (Propeller) erzeugt eine Liste mit Buchungsentwürfen (Positionen).

Die gewünschten Zeilen (Positionen) markieren, "Ausführen" erzeugt die Abschreibungsbuchungssätze.

### Anlagegut aktualisieren
Dies wird zum Beispiel für Sonderabschreibungen gebraucht:

+ Restwert aus dem Reiter "Historie" der regulären Abschreibung übernehmen, 
+ diese Zahl als "Wert" eintragen

Assistent "Abschreibungsbuchungssätze (Positionen) erstellen" ausführen.

## Kostenstellen
*erforderliches Modul: trytond_analytic_account*

## Budgetierung
*erforderliches Modul: trytond_analytic_budget*

## Mahnwesen
*erforderliches Modul: trytond_account_dunning*

Hierzu existieren auch noch einige Submodule zum Mahnen per email, zur Erzeugung von Mahnschreiben und zur Erhebung von Mahngebühren.

## Bruttopreismodul
*erforderliches Modul: trytond_trytond_account_tax_cash*

Dieses Modul erlaubt die Eingabe von Brutto- anstelle der üblichen Nettopreise bei den Artikeln.

## TSE-Kasse - Ladenverkauf (POS)
*erforderliches Modul: trytond_sale_point*

Mit Tryton lässt sich eine komplette Ladenkasse unter Verwendung von Standard-Hardwarekomponenten aufbauen. Details dazu unter [https://tryton-dach.org/tse-ladenkasse](https://tryton-dach.org/tse-ladenkasse).

## Webshop-Anbindungen
Tryton kann natürlich mit allen webshops kommunizieren. In den Kernmodulen enthalten sind Schnittstellen für vue storefront und shopify: 

*erforderliche Module: trytond_web_shop_shopify, trytond_web_shop_vue_storefront*

Weitere Module existieren, zum Beispiel für [woocommerce](https://discuss.tryton.org/t/woocommerce-connector-for-tryton/2795). Für andere Webshops ermpfielt sich eine Forumssuche oder -anfrage.


