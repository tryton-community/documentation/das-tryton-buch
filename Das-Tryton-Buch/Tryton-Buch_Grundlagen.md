# Einführung

Unser Tryton-Buch ist die erste umfassende Anwenderdokumentation für das ERM/ERP-System "Tryton".

Zielgruppe dieser Publikation sind insbesondere (technisch etwas versierte) Endanwender, denen wir eine Hand reichen wollen, um selbst ein Tryton-ERP-System aufzusetzen und die ersten Schritte der Einrichtung zu gehen. Bislang war Tryton praktisch nur für Anwender mit umfassenden Programmierkenntnissen nutzbar, weil die Anwenderdokumentation extrem knapp – und außerdem nur auf englisch verfügbar war. Das Tryton-Buch schließt diese Lücke und macht Tryton, das insbesondere auf dem deutschen Markt ein im Verhältnis zu seiner Qualität, Flexibilität und seinen Fähigkeiten absurdes Nischendasein führt, einem größeren Nutzerkreis zugänglich.

Bei der Komplexität der Aufgabe "Unternehmenssoftware" ist einfache und intuitive Bedienung realistischerweise kaum möglich. Der Nutzer muss mit einigem Einarbeitungsaufwand rechnen und sich darauf einstellen, gelegentlich hilflos vor der Bedienoberfläche zu sitzen. Wenn dann selbst dieses Buch keine Antwort mehr liefert,

* gibt es ein höchst aktives und hilfsbereites [Nutzerforum](https://discuss.tryton.org),
* ist auch deutschsprachige professionelle Unterstützung verfügbar.

Die Nutzung von Tryton setzt Grundkenntnisse im Umgang mit ERP-Systemen voraus, je nach Nutzung zum Beispiel in Buchhaltung, Warenwirtschaft, Zeiterfassung etc voraus, die hier nicht vermittelt werden können. Es gibt dazu eine Unzahl von Kursen, Büchern und Internet-Seiten, zum Beispiel diese: [https://buchhaltung-einfach-sicher.de](https://buchhaltung-einfach-sicher.de). Unser Buch kann nur die Tryton-spezifischen Aspekte beleuchten.

Nicht nur wegen des Arbeitsumfangs, sondern auch um den kooperativen Geist der Tryton-Gemeinschaft zu unterstreichen, haben wir uns entschieden, dieses Buch gemeinsam zu erarbeiten.

Das Autoren-Team:

Carina Werner, Frederik Jaeckel, Jan Grasnick, Korbinian Preisler, Hartmut Goebel, Richard Martin und Wolf Drechsel

Das Tryton-Buch ist **zweiteilig** angelegt:

* Im ersten Teil wird der Einstieg in Tryton beschrieben. Auf dieser Grundlage kann ein kleines oder mittleres Handels- oder Dienstleistungsunternehmen seine grundlegenden Geschäftsabläufe abbilden.
* Der zweite Teil geht in die Details und behandelt Spezialfragen, er ist insofern eher als Nachschlagewerk gedacht denn zum tatsächlichen Durchlesen.

## Was ist Tryton und wofür ist es nützlich?

Tryton ist eine Unternehmenssoftware, auch ERP- oder ERM-System (Enterprice Resource Planning/Enterprice Resource Management) genannt. Es kann einen großen Teil der in einem Unternehmen anfallenden Aktivitäten erfassen und verwalten und in Bezug auf die finanziellen Auswirkungen abbilden. Kernthemen dabei sind **Buchführung** und **Warenwirtschaft**. Tryton ist vollständig *OpenSource*, freigegeben unter der GNU General Public License Version 3 (GPLv3). Die Vorteile dieses Ansatzes sind:

* es kann stets entgeltfrei genutzt werden,
* es arbeitet mit offenen Datenformaten – alle Daten können stets eingesehen, exportiert und in anderen Programmen weiterverarbeitet werden
* eine Insolvenz des Herstellers kann nicht auftreten - wenn ein Mitglied der internationalen Entwicklergemeinschaft ausfällt, kann seine Arbeit von anderen übernommen werden.
* mittels API können andere Programme mit Tryton oder Tryton mit anderen Programmen standardisiert Daten austauschen

Alle x.0-Versionen von Tryton werden mit LTS (Long Term Support) zur Verfügung gestellt, d.h. diese werden mindestens 3 Jahre weiter gepflegt. Mit Stand dieser Veröffentlichung (Okt. 2022) ist die neueste Version die 6.6, die LTS-Version 6.0. Zum Einstieg von Tryton empfehlen wir jeweils die aktuellste LTS-Version zu wählen, da diese äußerst stabil läuft. Nur am Rande sei vermerkt, dass die Versionen mit ungeraden "Subnummern" wie z.B. 6.1 oder 6.3 rein zu Entwicklungs- und Testzwecken Verwendung finden. In der Regel ist alle 6 Monate mit einen neuem Release zu rechnen.

(*Die folgenden Angaben wurden zum Teil der Wikipedia entnommen:*) Die erste Version von Tryton wurde am 18. November 2008 veröffentlicht. Dabei handelte es sich um einen Fork der Version 4.2 von Tiny ERP (später OpenERP, heute Odoo). Client und Server sind in Python geschrieben, wobei der Client für die grafische Darstellung auf das GUI-Toolkit GTK+ zurückgreift. Tryton besteht aus dem Tryton-Kernel sowie einer Reihe ERP-spezifischer Module, die auf den Kernel aufsetzen. Der Tryton-Kernel ist ein dreischichtiges, universell verwendbares Software-Framework. Die drei Schichten des Frameworks sind:

+ Tryton Client 
+ Tryton Server
+ Tryton Datenbank, in der Regel PostgreSQL

Dieses Framework umfasst Funktionalitäten wie z.B. Datenintegrität, Modularität, Benutzerverwaltung mit Zugriffskontrolle und die Handhabung zeitgleicher Zugriffe auf Ressourcen. Damit stellt es eine vollwertige Applikationsplattform dar, welche für jeglichen gewünschten Zweck verwendet werden kann und nicht nur ausschließlich als ERP. Ein populäres Beispiel für ein anderes Projekt, welches auf diesem Framework aufsetzt ist GNU Health, ein Gesundheits- und Krankenhaus-Informationssystem.

### Anwendungsfälle

Tryton hat ein extrem breites Anwendungsspektrum. Seine enorme Leistungsfähigkeit erlaubt den Einsatz auch in größeren und großen Unternehmen. Da aber keine Lizenzgebühren anfallen, können auch Klein- und Kleinstunternehmen Tryton verwenden. Letztere haben dabei den Vorteil, dass die Software beim Wachstum ihres Unternehmens mithält und aufwändige Plattformwechsel nicht notwendig werden.

Tryton ist *plattformunabhängig*. Linux-, MacOS- und Windows-Rechner können nach Belieben zusammenarbeiten.

Tryton ist hochgradig *modular* aufgebaut, für unternehmensspezifische Spezialanforderungen können entsprechende Module programmiert werden. So kann Tryton zum Beispiel:

* Webshop-Anbindungen
* Zeiterfassung
* Kassensysteme

Der der flexible modulare Aufbau führt dazu, dass Fenster bestimmter Funktionen (durch individuelle Module/Erweiterungen) unterschiedlich aussehen können; Module können zusätzliche Felder oder Funktionen ergänzen. Die “Kunst” individueller Module liegt u.a. darin diese so zu entwickeln, dass sich diese möglichst wartungsfreundlich hinsichtlich künftiger Updates der Kernmodule sind.

Beispiel:

Abgebildetet sind (1) die Standard “Partei”-Ansicht und (2) die “Partei”-Oberfläche mit den Erweiterungen zu verschiedenen Felderweiterungen, dem vcard-Export und einem “Firmenlink”, um verschiedene Mitarbeiter/Standorte einer Firma zu verbinden. 

### Grenzen

Tatsächlich sind dem Leistungsumfang von Tryton keine wirklichen Grenzen gesetzt. In einigen Teilbereichen ist aber wegen der Dynamik der Entwicklung/Anforderungsänderungen/Vielfalt eine integrierte Entwicklung (betriebs)wirtschaftlich nicht darstellbar. Als Beispiele hierfür seien **Lohnbuchhaltung**, **OnlineBanking** und **eBilanz** genannt. Hier existieren Schnittstellen, um auf das Know-how von Drittanbietern zugreifen zu können.

# Erster Teil - Einstieg in Tryton

## Für ungeduldige - Tryton ausprobieren

Unter <https://demo.tryton.org> steht eine Testinstanz von den Kernentwicklern von Tryton zur Verfügung. Nutzername und Kennwort sind jeweils *demo*. Die Datenbank wird stündlich zurückgesetzt.

Dominique Chabord (Forum-Benutzername "sisalp") stellt unter <https://sisalp.com> kostenlos einen fertig installierten standardisierten Testserver zur Verfügung. Auf Wunsch überläßt er dem Benutzer die Datenbank.

## Struktur

Tryton ist als 3-Schichten-Architiktur konzipiert, es besteht aus:

* der PostgreSQL-Datenbank,
* dem Tryton-Framework (Server und Dämon) und
* dem Klienten (Tryton-Client oder Webinterface).

Die Datenbank und der Tryton-Server laufen (in der Regel) auf dem gleichen Rechner, der Klient kann, muss aber nicht räumlich davon getrennt sein. Es können mehrere Klienten (und damit Benutzer) gleichzeitig auf einen Server zugreifen, Tryton läuft aber genauso gut auf einem einzelnen Rechner. Durch diese Flexibilität kann Tryton problemlos mit dem Wachstum eines Unternehmens mithalten.

### Datenbank

In der Datenbank werden alle Personen- und Unternehmensdaten und alle Vorgänge gespeichert. Verwendet werden können verschiedene Datenbanksysteme, am verbreitesten ist das freie Datenbanksystem "PostgreSQL", auf das wir uns im folgenden stets beziehen. 

### Server und Dämon

Diese bestehen aus

* dem Kern des Programms (Dateiname `trytond`) und den Basismodulen `iw` und `res`. Sie sind in jeder Tryton-Installation vorhanden; sowie
* den Modulen (Dateinamen `trytond-[Name-des-Moduls]==[Versionsnummer]`). Sie erweitern die Fähigkeiten des Programms benutzerspezifisch.

Der Dämon ist die zentrale Kommunikationsinstanz zwischen dem Benutzer, dem Framework und der Datenbank.

### Klient

Hier passieren die Benutzereingaben. Als Klient fungiert entweder

* ein für Linux, MacOS und Windows verfügbarer Desktop-Client (Dateiname: "tryton") - oder
* ein Web-Browser

Beide Lösungen sind in ihrem Funktionsumfang identisch und im Bedienkonzept sehr ähnlich, die Klient-Software bietet einige zusätzliche Komfort-Funktionen.

Für den Zugriff mit Web-Browser ist die Installation zusätzlicher Software, dem "sao-Client" nötig.

## Tryton as a service (SaaS)

Tryton kann auch als Service für ein monatliches Entgelt “gebucht” werden. Der Zugriff wird über das Internet auch hier sowohl über den Client oder den Browser möglich. Der Vorteil für den Anwender: Die Konfiguration, Wartung/Pflege, Erweiterung(en), Sicherheit und Backups macht der Provider. Angeboten wird dieser Service unter anderem in Deutschland von virtualThings und martin - data services, in der Schweiz von tryton.cloud und in Frankreich von SISalp. 

## Installationsmöglichkeiten

Die Installation des Tryton-Servers ist keine triviale Angelegenheit; wer im Umgang mit der Konsole und den Grundbegriffen gar nicht vertraut ist, sollte besser Beratung in Anspruch nehmen.

Unter der Vielzahl von Möglichkeiten greifen wir hier diejenigen heraus, die wir in kleineren Installationen für naheliegend halten. Möglich sind auch ungleich komplexere Szenarien als die hier dargestellten. Hierfür ist aber gründliche Planung und fachkundige Beratung notwendig, dieses Buch muß hier passen.

Folgende Möglichkeiten sind - neben vielen anderen - gängig:

+ Installation in virtuellen Umgebungen, skriptgeführt oder manuell
+ Installation in einem Docker- oder Podman-Container,
+ auf Debian- oder Ubuntu-Systemen: Installation direkt ins System, unter Verwendung des apt-Paketverwalters

Unabhängig von der Installationsart ist die Datenbank-Struktur ist stets die gleiche, damit kann nach Belieben von einer Installationsart auf eine andere gewechselt werden. Ein naheliegendes Szenario wäre, eine Installation in einer virtuellen Umgebung anzulegen, einzurichten und zu testen und sie dann nach Docker umzuziehen. Dort ist sie für den vorgesehenen Nutzerkreis verfügbar und gleichzeitig gut vor schädlichen Einflüssen geschützt.

Wir empfehlen die Anlage mindestens von

* einer *experimentellen* Version – hier kann man Aktionen ausprobieren, mit denen man noch nicht ausreichend vertraut ist. Das ist deswegen wichtig, weil sich viele Aktionen in Tryton nicht rückgängig machen lassen.
* eine *produktiven Version* – hier findet die eigentliche Buchungs- und Verwaltungsarbeit statt, und
* die Aufbewahrung älterer Versionen. Bei einem großen Update sollte man die Vorgängerversion noch eine Zeitlang aufbewahren, bis man sicher ist, daß die neue problemlos funktioniert.

Jede Installation greift normalerweise auf ihre eigene Datenbank zu; alle diese sind in einem *PostgreSQL Datenbank-Server* beheimatet. Die meisten Komponenten von Tryton sind im ["Python Package Index" (PyPi)](http://pypi.org) zu finden, zu deren Nutzung wird auf PIP, den "package installer for python" zurückgegriffen.

### Skriptgeführte Installation für Python virtuelle Umgebungen (Ubuntu-Linux u.ä.)

Für Installation von Wartung von Tryton mit PIP in virtuellen Umgebungen stellen wir einen Satz Skripte sowie vorbereitete Modul-Listen für Ubuntu-Linux (verwendbar auch auf Debain, Mint etc) zur Verfügung. Die Skripte werden laufend ergänzt und verbessert und sind hier verfügbar:

<https://gitlab.com/TrytonDACH/tryton-installation-and-maintainance-scripts-for-pip>

Die Autoren freuen sich über alle Arten von Beiträgen wie Adaptierungen für andere Betriebssysteme, Fehlermeldungen, Skripte für andere Zwecke etc.

Das Installations-Skript wird mit dem Befehl `$ bash Installations-Skript-Tryton.sh` aufgerufen, es fragt den Installationsort und die zu vergebenden Namen ab und ist weitgehend selbsterklärend. Selbstverständlich kann es auch als Auflistung der nötigen Befehle genutzt werden.

Die Befehle sind im [Anhang](#trytonfuerpipinstallieren) wiedergegeben, ambitionierte Benutzer können damit die Installation von Hand vornehmen oder auftretende Fehler beheben.

### Manuelle Installation (alle Systeme) 

Hier findet sich eine Aufstellung der Befehle für Debian-basierte Systeme, für andere UXe muß diese etwas angepaßt werden:

[Befehlsliste Tryton-Server-Installation](https://www.tryton-dach.org/tryton-buch-administrator/#Installation_auf_ubuntu-Linux)

Und hier steht eine angepaßte Anleitung für MS Windows. Wir haben wenig Erfahrung mit Win-Servern und sind hier besonders auf Rückmeldung angewiesen:

[Installationsanleitung für MS Windows](https://www.tryton-dach.org/tryton-buch-administrator/#Installation_Server_und_Client_auf_MS_Windows)

### Docker (alle Systeme)

Für den Einstieg raten wir denen, die mit Docker nicht gut vertraut sind, von dieser Möglichkeit ab. Es ist nicht trivial, sich in einer Docker-Umgebung zurechtzufinden. Wer diesen Weg wählen will, findet [hier eine umfassende Anleitung von Dave Harper.](https://discuss.tryton.org/t/how-to-run-tryton-using-docker/3200)

### Installation im Server-Betriebssystem (Debian oder Ubuntu)

Für Debian und Ubuntu stehen direkt installierbare Pakete zur Verfügung, die über die Paketmanager eingespielt werden können. Die kann auch auf virtuellen Maschinen geschehen, die Installation ist schnell und einfach und stellt einen sehr stabilen Betrieb sicher. Allerdings ist nur die jeweils aktuelle LTS-Version verfügbar. Unter [](https://tryton-team.pages.debian.net/) finden sich weitere Details.

## Vor- und Nachteile der Installationsmethoden

Die folgende Tabelle versucht, die Vor- und Nachteile der Installationsmethoden zu bewerten. Sicher kann man hier - je nach Schwerpunktsetzung und Philosophie - zu unterschiedlichen Resultaten kommen. Da der Wechsel zwischen den Methoden mit überschaubarem Aufwand möglich ist, muß man die Entscheidung für eine Methode nicht überbewerten.

| |im Server-Betriebssystem (Debian- oder Ubuntu-Pakete)|in virtuellen Umgebungen|in Docker- oder Podman-Containern
:--- | :---: | :---: | :---:
**Installation** | sehr einfach | sehr komplex, einfach mit Installationsskript | einfach
**Wartung des Systems** | sehr einfach | sehr komplex, einfach mit Wartungsskript | komplex
**Aktualisierung des Systems** | einfach, aber nur LTS-Versionen möglich | sehr komplex, einfach mit Update-Skript | komplex
**mehrere Tryton-Versionen auf einem System möglich** | nein (nur mit virtuellen Maschinen)| ja | ja, eine pro Container
**aktuellste Tryton-Version möglich** | nein, nur LTS | ja | ja
**skalierbar** | ja | ja | ja
**betriebssystemunabhängig** | nein | ja | ja
**Verbrauch von Plattenspeicher** | klein | klein | groß

### Docker

Für den Einstieg raten wir denen, die mit Docker nicht gut vertraut sind, von dieser Möglichkeit ab. Es ist nicht trivial, sich in einer Docker-Umgebung zurechtzufinden. Wer diesen Weg wählen will, findet [hier eine umfassende Anleitung von Dave Harper.](https://discuss.tryton.org/t/how-to-run-tryton-using-docker/3200)

### Direkt ins System (Debian und Ubuntu)

[Hier finden sich](https://wiki.ubuntuusers.de/Archiv/Tryton/)  - allerdings nicht mehr sehr aktuelle Hinweise für Ubuntu, [hier deutlich besser gepflegte](https://tryton-team.pages.debian.net/mirror.html) für Debian. 

### Den Klienten installieren

Tryton kann auch über einen Webbrowser aufgerufen werden. Dazu muß auf dem Server allerdings ein eigener Webserver laufen, dessen Installation wird über das Installationsskript nicht abgedeckt. Wir empfehlen stattdessen die Installation des Desktop-Klients.

Der Klient für ihr System kann unter <https://downloads.tryton.org/>[Ihre-Version] heruntergeladen werden. Es handelt sich um die Dateien

* tryton-[Versionsnummer].exe (für Windows)
* tryton-[Versionsnummer].tar.gz (für Linux) und
* tryton-[Versionsnummer].dmg (für MacOS)

Die Datei `tryton-*` ist dabei der Klient, die `trytond-*`-Serie der Dämon und darf nicht mit dem Klienten verwechselt werden.

Die Hauptversionsnummer (x.y.*) muß mit der Version Ihres Dämons übereinstimmen; die dritte Ziffer darf abweichen.

Unter Linux empfehlen wir, den Linux-Klienten in die jeweilige virtuelle Umgebung zu installieren; unser Installationsskript erledigt das gleich mit. Er kann auch heruntergeladen und direkt durch Aufruf der Datei tryton-[Versionsnummer]/bin/tryton gestartet werden.

Beim ersten Einloggen muß sich der Klient mit dem Server verbinden. Dazu im Startdialog "verwalten" klicken, mit dem "+" ein neues Profil hinzufügen, als Servername "localhost" eintragen, die eingerichtete Datenbank aus dem Aufklappmenu auswählen. Der erste Aufruf erfolgt mit dem Benutzernamen "admin", das voreingestellte Kennwort unserer Musterdatenbank lautet "123456789".

## Den Tryton-Client kennenlernen

Dieses Kapitel ist für die Leute, die sich von Grund auf in Tryton einarbeiten möchten. Wer schnellstmöglich zu Resultaten kommen will, kann gleich zum Kapitel "Tryton einrichten" springen und gelegentlich auf dieses Kapitel zurückgekommen und hier nachschlagen.

### Das User Interface

Hier ist ein Überblick über das Client Interface, das Web Interface sieht sehr ähnlich aus.

![Das Hauptmenü](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/programmfensterpcclient.png?fileId=23188#mimetype=image%2Fpng&hasPreview=true)

![Einstellungen](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/hauptmenue.png?fileId=23185#mimetype=image%2Fpng&hasPreview=true)

Zugang zu den Einstellungen für den eigenen Nutzeraccount (abhängig von Ihren Rechten), wie z.B. Sprache, E-Mailadresse, Autostartseiten.

*Werkzeugleiste*

Ändert die Ansicht der Werkzeugleiste: als Symbol(e)/Text(e)/...

*Formular*

Hier können Sie Ihre persönliche Präferenzen für die Eingabemasken einstellen

*PDA*

Hier können Sie den optimierten Modus für PDAs/Tablets aktivieren

*Suchlimitierung*

Hier können Sie eine Begrenzung für die Länge der Trefferlisten (bei Suchen/Filtern) festlegen.

*E-Mail*

Legt die Kommandozeile für das Starten des E-Mail Clients zum Versenden einer Mail fest.

*Nach neuer Version suchen*

De-/Aktiviert die automatische Suche nach Updates für das Clientprogramm.

*Schnelltasten*

Hier finden Sie einen Überblick über Shortcuts/Tastenkürzel

*Über*

Hier finden Sie Infos über Ihre Tryton Version und Mitwirkende am Tryton-ERP

*Beenden*

Hiermit schließen Sie das Eingabeinterface

*Ein-/Ausschalten der Navigationsmenüs*

Ein Klick schließt/öffnet die Ansicht des Navigationsmenüs.

![Die Liste der Favoriten](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/favoritenliste.png?fileId=23197#mimetype=image%2Fpng&hasPreview=true)

Als Favoriten können häufig genutzte Programmfunktionen gespeichert werden. Einen Favoriten hinterlegen Sie durch das Markieren des Sterns rechts neben dem Eintrag im Navigationsmenü. Ist der Stern vollflächig blau, ist der Eintrag als Favorit hinterlegt. Durch erneutes Anklicken des Sterns wird die Favoriteneigenschaft deaktiviert.

![Schnellsuche/Navigation zur Eingabemaske](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/schnellsuchenav.png?fileId=23191#mimetype=image%2Fpng&hasPreview=true)

Bei Eingabe des Suchbegriffs werden die gesuchten Menüpunkte gelistet.

*[zu 5] Minimieren der Oberfläche*

Minimiert das Programm.

*[zu 6] Maximieren/Skalieren der Eingabemaske*

Maximiert/Skaliert die Größe der Programmoberfläche.

*[zu 7] Schließen/Beenden des Tryton Clients*

Beendet das Programm.

*[zu 8] Navigationsmenü*

Der Navigationsbereich gibt einen Überblick über die verfügbaren Formulare. Das gewünschte Formular kann durch Doppelklick geöffnet werden, es erscheint als neuer Reiter oder wird, sofern schon geöffnet, in den Vordergrund geholt.

#### Das User Interface – Formular- und Listenansicht

![Formular- und Listenansicht](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/formularlistenansicht.png?fileId=23200#mimetype=image%2Fpng&hasPreview=true)

*[a] Reiterleiste mit den geöffneten Formularen*

Das gerade geöffnete/aktive Formular kann man an der Unterstreichung erkennen.

*[b] Bezeichnung des aktiven Formulars*

*[c] Die Symbol-/Werkzeugleiste*

Hier in der Ansicht "Standard" (nur als Symbole). Die Ansicht kann im Menü unter „Werkzeugleiste“ (s.o.) geändert werden.

*[d] Ein vollflächiger Stern*

Bedeutet: als Favorit in der Favoritenliste gespeichert

*[e] Das Eingabefeld zur Suche/zum Filtern der im Formular gelisteten Objekte*

*[f] Liste der Objekte (wie z.B. Produkte, Rechnungen, Lieferscheine, Adressen, usw.)*

#### Das User Interface – Bedienknöpfe in Formularen

Viele der Bedienknöpfe im Kopfbereichs des Formulars finden sich im Formular wieder, dort beziehen sie sich auf das entsprechende Unterformular.

![Bedienknöpfe in Formularen](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/bedienknoepfe.png?fileId=23194#mimetype=image%2Fpng&hasPreview=true)

#### Die Werkzeugleiste

Ansicht der Werkzeugleiste in der Darstellung „Text und Symbole“.

![Die Werkzeugleiste](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/werkzeugleiste.png?fileId=20338#mimetype=image%2Fpng&hasPreview=true)

**Ansicht wechseln**

Eine überaus wichtige Funktion! - Mit dieser Schaltfläche wechseln Sie zwischen Formular- und Listenansicht eines Datensatzes zum Beispiel bei Produkt, Rechnung, Lieferschein, Partei etc. Die Formularansicht zeigt dabei die Details eines Eintrags, die Listenansicht alle Einträge.

**Vorheriger**

Wechselt zum vorherigen Datensatz. Dieses funktioniert sowohl in der Listen- als auch Detail-/Formularansicht. Falls nicht gespeicherte Eingaben vorliegen, kommt eine Nachfrage zum Speichern.

**Nächster**

Wechselt zum nächsten Objekt/Datensatz. Dieses funktioniert sowohl in der Listen- als auch Detail-/Formularansicht.

**Neu**

Erstellt ein neues Objekt/einen neuen Datensatz. Dieses funktioniert sowohl in der Listen- als auch Detail-/Formularansicht.

**Speichern**

Speichert das aktuelle Objekt/den aktuellen Datensatz. Sie können auch mit der Tastenkombination Strg+S speichern.

**Neu laden/Rückgängig**

Lädt den Inhalt des aktuellen Formulars aus der Datenbank nach. Bei nicht gespeicherten Eingaben erscheint eine Nachfrage zum Speichern. Sollte ein anderer Mitarbeiter zur gleichen Zeit Änderungen vornehmen, werden diese nun bei Ihnen auch aktuell angezeigt. Desgleichen können Sie, solange Sie nicht "Speichern" gedrückt haben, Ihre Änderungen am aktuellen Datensatz verwerfen.

**Anhänge**

Zu jedem Objekt/Datensatz können Sie beliebig viele Dokumente (z.B. pdf, jpg, docx, usw.) ablegen. Die Schaltfläche ermöglicht das Hinterlegen neuer Dokumente und das Verwalten der bereits hinterlegten Dokumente. Die abgelegten Dokumente werden auf den Tryton Server hochgeladen und werden dort in einem Ordner gespeichert (nicht in der Datenbank).

**Notiz(x)**

Hier können Sie Notizen zu dem markierten oder geöffneten Datensatz hinterlegen, nachlesen und verwalten.

**Aktion**

Bei dieser Schaltfläche können verschiedene Funktionen hinterlegt sein, die auf das geöffnete Objekt angewendet werden können. Die entsprechenden Funktionen werden durch den Administrator hinterlegt und zur Verfügung gestellt.

**Beziehung**

Zeigt Datensätze an, die mit dem aktuellen Objekt/Datensatz in Beziehung stehen.

**Bericht**

Erzeugt für den oder die markierten Datensätze den ausgewählten Bericht. Sie können in der Listenansicht mehrere Datensätze auswählen (Strg+Klick) und für diese den Bericht abrufen.

**E-Mail**

Erzeugt den ausgewählten Bericht und öffnet das lokale E-Mailprogramm mit einer neuen E-Mail und dem Bericht als Anhang. Ihre E-Mail Software sollte sich hierzu automatisch öffnen.

**Drucken**

Erzeugt den ausgewählten Bericht und startet den Druck des Berichts auf dem Standarddrucker.

**URL kopieren**

Zeigt den URL der aktuellen Ansicht. Der URL enthält Informationen zum aktuellen Formular und geöffnetem Datensatz. Sie können den URL anklicken, um ihn in die Zwischenablage zu kopieren. Diese Info kann für den Service nötig sein.

#### Das Werkzeugmenü

![werkzeugmenue.png](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/werkzeugmenue.png?fileId=20395#mimetype=image%2Fpng&hasPreview=true)

Links neben dem Feld mit der Bezeichnung des aktuellen Formulars befindet sich eine Schaltfläche mit einem kleinen Dreieck bzw. Pfeil . Hier sind verschiedene Menüpunkte hinterlegt. Die meisten Menüpunkte erkennen Sie aus der Symbolleiste wieder.

Zusätzliche, noch nicht besprochene Funktionen sind:

**Duplizieren**

Erstellt eine Kopie des aktuellen Datensatzes.

**Löschen**

Löscht den aktuellen Datensatz. Je nach Datensatztyp werden dabei auch alle verbundenen Daten gelöscht. Bitte beachten Sie, dass diese Funktion nicht auf festgeschriebene Objekte angewandt werden kann, da dieses den vorgeschrieben Archiv- und Dokumentationspflichten widersprechen würde.

**Protokoll ansehen**

Zeigt technische Details zum aktuellen Datensatz an. Wenn die Datenbank-Historie für den aktuellen Datensatz aktiv ist, erscheinen hier auch alle bisherigen Änderungen.

**Daten exportieren**

Öffnen den Export-Dialog. Hier können Standard-Filter zum Export hinterlegt werden und die Daten im .csv-Format exportiert werden.

**Daten importieren**

Hinsichtlich des Imports müssen sehr genaue Einstellungen vorgenommen werden. Die Import-Funktion ist aktuell (2022) eine der größten Schwachstellen von Tryton, daher werden Importe kein Teil Ihrer gewöhnlichen Arbeit sein.

**Registerkarte schließen**

Diese Funktion schließt das aktuelle Formular/Modul.

#### Die Navigationsleiste

![navigationsleiste.png](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder/navigationsleiste.png?fileId=20434#mimetype=image%2Fpng&hasPreview=true)

Bitte beachten Sie, dass die Navigationsleiste je nach zugewiesenen Aufgaben und Rechten des Benutzers unterschiedliche Elemente umfasst und daher vom Umfang stark variieren kann. Das Aussehen der abgebildeten Navigationsleiste dient lediglich als Beispiel.

### Suche nach Datensätzen

Jede Listenansicht in Tryton enthält auch eine Suchzeile. Hier können Suchbegriffe eingegeben werden. Dadurch wird die Datensatzliste gefiltert und die Anzahl der angezeigten Datensätze verringert sich.

Ohne Suchbegriffe in der Suchzeile werden alle Datensätze angezeigt.

![Ohne Suchbegriff](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchfeldohne.png?fileId=20719#mimetype=image%2Fpng&hasPreview=true)

Bei einer einfachen Suche wird nur der Suchbegriff eingegeben, danach 'Enter' drücken.

![Einfache Suche](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/einfsuchename.png?fileId=20479#mimetype=image%2Fpng&hasPreview=true)

Die einfache Suche wird im Datensatznamen ausgeführt. Der Datensatzname ist intern vordefiniert. Im Allgemeinen ist dies das Feld Name oder ein vergleichbares Feld. Die Suche kann auch in mehreren Feldern gleichzeitig stattfinden. Beim Datentyp Partei wird in Name und Code Partei gesucht.

![Suche nach bestimmten Feldinhalten](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/einfsuchenr.png?fileId=20482#mimetype=image%2Fpng&hasPreview=true)

Die Eingabe einer Kunden-Nr. liefert also auch ein Ergebnis.

Um nach dem Inhalt von bestimmten Feldern zu suchen, geben Sie den Spaltentitel des Feldes (oder ein paar Zeichen vom Beginn des Titels) ein und warten kurz. Tryton schlägt passende Feldnamen vor.

![Suche mit Vorschlag des Suchbegriffs](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/sucheinhvorschlag.png?fileId=20497#mimetype=image%2Fpng&hasPreview=true)

Klicken Sie den gewünschten Eintrag an, dadurch wird der Spaltentitel in die Suchzeile geschrieben. Alternativ können Sie auch den vollständigen Spaltentitel und einen Doppelpunkt dahinter eintippen. Hinter den Doppelpunkt schreiben Sie den Suchbegriff und drücken <Enter> .

![Suche mit Vorschlag des Suchbegriffs, in Ausführung](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/sucheinhalt.png?fileId=20491#mimetype=image%2Fpng&hasPreview=true)

Die Suche kann gleichzeitig nach mehreren Feldinhalten erfolgen. Beginnen Sie wie bei einer Suche nach nur einem Feld, siehe oben. Setzen Sie hinter dem ersten Suchbegriff ein Leerzeichen in die Suchzeile. Tryton schlägt weitere Felder zur Suche vor.

![Suche nach mehreren Feldinhalten](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchemehrfelder.png?fileId=20587#mimetype=image%2Fpng&hasPreview=true)

Hierbei werden alle Felder des aktuellen Datensatztyps vorgeschlagen, für die eine Suche möglich ist. Auf der Partei kann man also nicht direkt nach Adressen suchen, verwenden Sie dafür den Menüpunkt Adressen.

![Suche mit verknüpften Suchbegriffen](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/sucheverkn.png?fileId=20668#mimetype=image%2Fpng&hasPreview=true)

Die Suchbegriffe werden UND-Verknüpft; das heißt, es werden nur Datensätze ausgegeben, bei denen alle Suchbegriffe zutreffen.

#### Suche nach Werten in einem Bereich

Wenn Felder numerische Werte oder Datumsangaben enthalten, können Sie mit genauen Werten oder Wertebereichen suchen.

![Beispielsuche nach einem genauen Wert, hier ist es ein Datum.](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchegenwert.png?fileId=20488#mimetype=image%2Fpng&hasPreview=true)

![Suche nach einem Bereich, Verkaufsdatum liegt vor dem 06.04.2020](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchekleinerals.png?fileId=20500#mimetype=image%2Fpng&hasPreview=true)

![Hier gibt es keinen Treffer, da die Verkäufe genau am 06.04.2020 waren und nicht davor](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchekleineralsohne.png?fileId=20494#mimetype=image%2Fpng&hasPreview=true)

![Suche nach einem Bereich, Verkaufsdatum liegt vor oder am 06.04.2020](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchekleinergleich.png?fileId=20503#mimetype=image%2Fpng&hasPreview=true)

![Verkaufsdatum liegt zwischen dem 01.04.2020 und dem 08.04.2020, jedoch nicht genau auf diesen Datumsangaben](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchevonbis.png?fileId=20692#mimetype=image%2Fpng&hasPreview=true)

#### Komplexe Suchanfragen - UND-Verknüpfung

Sie können nach mehreren Feldern suchen und dabei logische Verknüpfungen festlegen.

![Suche nach 'ch' im Namen einer Partei mit Kunden-Nr '0002' oder '0003'](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchekomplex.png?fileId=20578#mimetype=image%2Fpng&hasPreview=true)

Die logische Verknüpfung lautet hier also:

Ergebnis = (Name enthält 'ch') UND ((Code Partei ist '0002') ODER (Code Partei ist '0003'))

Das Semikolon in einem Suchbegriff ist ein Listentrenner und erzeugt beim Suchen eine Liste von möglichen Werten für ein Feld.

Eine Liste von möglichen Treffern wird immer als ganzes Wort gesucht. Eine Suche im Feld Name mit Name: ch;tr führt zu keinem Ergebnis, da hierbei nach Name ist ch ODER tr gesucht wird. Die beiden Firmennamen sind jedoch länger. Um Treffer zu haben, müsste der Suchbegriff also Name: "Trolltech Inc.";"Schrauben Karl" lauten. Sie können auch mehr als zwei Felder mit mehr als nur zwei Möglichkeiten für ein Feld wie Code Partei in einer Suche verwenden.

#### Komplexe Suchanfragen - ODER-Verknüpfung

Um die Einträge zu finden, in denen zum Beispiel "Maier" ODER "Huber" ODER "mann" enthalten sind, muß der Sucheintrag wiederholt und mit dem Schlüsselwort "or" verknüpft werden:

`Name: Maier or Name: Huber or Name: "mann"`

würde also eine Liste mit "Maier, Huber, Neumann, Klausmann" liefern.

#### Suchbegriffe für spätere Wiederverwendung speichern

Sie können eine fertig formulierte Suche für eine spätere Verwendung in Tryton speichern. Klicken Sie dazu am rechten Ende der Suchzeile auf das Lesezeichensymbol.

![Suche speichern](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchesave.png?fileId=20590#mimetype=image%2Fpng&hasPreview=true)

Geben Sie eine Bezeichnung für die Suche ein und klicken Sie auf OK.

![Name eingeben - ist dieses Bild evt. entbehrlich??](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/suchesavename.png?fileId=20632#mimetype=image%2Fpng&hasPreview=true)

Um die gespeicherte Suche später wieder abzurufen klicken am rechten Ende des Suchfeldes auf das Lesezeichenlistensymbol. Wurden mehrere Suchen gespeichert, klappt eine Liste auf.

![Suche aufrufen](../wp-content/uploads/Den_Tryton-Client_kennenlernen/Bilder_Suche_nach_Datens%C3%A4tzen/sucheaufrufen.png?fileId=20485#mimetype=image%2Fpng&hasPreview=true)

Suchen werden für jeden Trytonnutzer getrennt gespeichert.

### Tastatur-Kurzbefehle

Eine Liste verfügbarer Kurzbefehle ist mit der Tastenkombination strg-F1 einsehbar; über die Titelzeile dieses Fensters sind weitere Seiten aufrufbar. Wichtige Kurzbefehle sind:

* Strg-K: Texteingabefeld oben links aktivieren
* Umschalt(shift)-Pfeil rechts: Liste (mit Keil) aufklappen (*ACHTUNG: Bei großen Listen kann dieser Befehl einige Zeit brauchen, er erschließt sich daher nicht von selbst*)
* Umsch-Pfeil links: Liste einklappen
* Strg-TAB: Zum nächsten Reiter wechseln
* Strg-Umsch-TAB: Zum vorherigen Reiter wechseln

Hinweise zur individuellen Anpassung von Tastaturbefehlen finden sich unter [Tastatur-Kurzbefehle editieren](#tastaturkurzbefehleeditieren).

## Tryton einrichten

### Allgemeine Hinweise

Viele Fenster in Tryton bieten eine erschreckende Zahl an Eingabemöglichkeiten. Dies ist nicht nur für den Einsteiger verwirrend, denn oft müssen nur wenige dieser Felder ausgefüllt werden. Viele Sachverhalte sind auf übergeordneter Ebene geregelt, diese Felder sind nur dazu da, Ausnahmen zu ermöglichen. Gerade Einsteiger sollten *sparsam* mit den Einträgen sein und zunächst nur diejenigen vornehmen, die sie wirklich für erforderlich halten. Beim Speichern des Fensters prüft Tryton, ob alle Pflichtfelder ausgefüllt sind und gibt entsprechende Hinweise zurück. Die Beschriftung von **Pflichtfeldern** ist oft, aber nicht immer **fett** gesetzt.

Einen Ausfüllhinweis für erforderliche Felder in einem anderen Fenster [geben wir hier.](#konfigurationvonhandvornehmen)

### Methoden der Gewinnermittlung

Hinsichtlich der Einrichtung von Tryton sind drei Möglichkeiten der Gewinnermittlung möglich:
- [1] Bilanzierung mit umsatzsteuerlicher Sollversteuerung
- [2] Bilanzierung mit umsatzsteuerlicher Istversteuerung
- [3] EÜR (Einnahme-Überschuss-Rechnung) 

Standardmäßig verwendet Tryton Variante [1] und ist damit "out of the box" für Kapitalgesellschaften einsetzbar. Kommt diese Variante zum Einsatz, sind bei der Konfiguration keine Besonderheiten zu beachten. Hier wird mit Datum der Rechnungsstellung wird eine Forderung erfolgswirksam verbucht und gleichzeitig entsteht die USt-Verbindlichkeit gegenüber dem Finanzamt.
Variante [2] ist u.a. bei kleineren GmbHs möglich, kommt aber seltener zum Tragen. Auch hier wird mit Datum der Rechnungsstellung eine Forderung erfolgswirksam verbucht aber die Umsatzsteuer-Schuld entsteht erst mit dem Eingang des Geldes.
Der Großteil der Unternehmen in Deutschland sind Einzelunternehmen/kleine Personengesellschaften und verwenden die EÜR [3]. Das heißt, erst mit dem Zahlungseingang wird die Leistung erfolgswirksam und die USt-Schuld entsteht auch erst zu diesem Zeitpunkt. Wer die Einnahmeüberschußrechnung (EÜR) anwendet, muss das Modul `trytond_account_tax_cash` aus dem Standard-Repositorium und in Deutschland zusätzlich das  `trytond_account_tax_cash_de` aus dem MDS-Repo installieren. Diese Module kümmern sich darum, dass Einnahmen und Steuern auf die richtigen Konten gebucht werden.

Bei Unsicherheit hinsichtlich der Soll/Istversteuerung sollte beim Finanzamt oder Steuerberater nachgefragt werden.

Grundsätzlich deckt Tryton alle drei Varianten ab.

### Wahl des Kontenrahmens

Auch wenn Tryton einen "Minimalen Kontenrahmen" anbietet, ist dringend zu empfehlen, einen der DATEV-Standardkontenrahmen zu verwenden. Auch wenn die Vielzahl deren Konten auf den Einsteiger abschreckend wirken mag, sind diese derart weit verbreitet, dass ihre Handhabung ungleich leichter fällt als der Umgang mit einem einfachen, selbsterstellten Kontenrahmen.

Derzeit verfügbar in Tryton sind:

+ SKR03 prozessorientiert gegliedert
+ SKR04 gegliedert nach dem Abschlussprinzip, für Bilanzierung oder EÜR
+ im Beta-Status: SKR49 für Stiftungen und Vereine - abgespeckt, für Bilanzierung oder EÜR

Andere Kontenrahmen können mit überschaubarem Aufwand eingerichtet werden. Im zweiten Teil des Buchs finden sich detaillierte Angaben für die Einrichtung “zu Fuß", die auch zur Anpassung unserer Muster-Datenbanken genutzt werden können.

### Vorlagen für Rechnung, Angebot, Lieferschein etc.

Alle Arten von Dokumenten, die von Tryton für die Außenwelt erzeugt werden, werden intern "Berichte" genannt.

![](../wp-content/uploads/icons/tryton-menu.svg) Verwaltung > Programmoberfläche > Aktionen > Berichte

Hier sind zunächst "Rechnung", "Einkauf", "Verkauf", "Pickliste" und "Lieferschein" relevant. Nach Doppelklick können im folgenden Fenster

![Bericht downloaden](../wp-content/uploads/Tryton_einrichten/Bericht-Beispiel.png)

die gespeicherte Berichtsvorlage mit dem Download-Symbol heruntergeladen, mit dem Kreuz-Symbol gelöscht, in Open/LibreOffice bearbeitet und die modifizierte Berichtsfassung mit dem Lupensymbol wieder in Tryton upgeloadet (speichern nicht vergesen) werden. Die Feldeinträge in den Open/LibreOffice-Dokumenten sind in [Genshi](https://genshi.edgewall.org) geschrieben.

Die Bearbeitung der Berichtsdateien ist ein komplexes Thema, vorläufig sollte sich der Anwender auf das Einbinden eines eigenen Briefkopfes beschränken. Im Downloadbereich von [Tryton Community](https://foss.heptapod.net/tryton-community/tools/berichtsvorlagen-deutschland) stellen wir Mustervorlagen für verschiedene Zwecke zur Verfügung. 

Natürlich sind praktisch beliebig komplexe Auswertungen in Text- und Tabellenform möglich. Details dazu siehe "Tryton für Fortgeschrittene". 

### Import / Export eigener Daten

Der Import eigener Daten ist derzeit auch für den erfahreneren Benutzer nur schwer durchzuführen. Hier besteht Handlungsbedarf seitens der Entwickler. Vor einem Import kann ein Export mit Muster-Daten zu empfehlen, so erhält man einen Beispiel, wie der Import aussehen sollte.

Datenexporte sind etwas einfacher durchzuführen. "![](../wp-content/uploads/icons/tryton-arrow-down.svg) > Daten exportieren" öffnet den entsprechenden Dialog, als Feldtrenner funktioniert in DE der Strichpunkt besser als das Komma. Exportvorlagen können recht einfach angelegt und gespeichert werden.

### Konfiguration von Hand vornehmen{#konfigurationvonhandvornehmen}

Der einfachste Weg Tryton zu konfigurieren ist normalerweise die Verwendung einer Musterdatenbank - siehe nächster Abschnitt.

Wer keine Musterdatenbank nutzen will oder kann, kann die im nächsten Abschnitt aufgeführten Einträge als "Checkliste" verwenden und die Einträge manuell vornehmen. Tryton bietet dabei eine clevere Hilfe: Sofern in einem Fenster Einträge aus einem anderen benötigt werden (zum Beispiel: Zum Anlegen eines Verkaufs ist eine Kundenpartei erforderlich), kann im betreffenden Feld die Leertaste gedrückt werden, es erscheint dann das Wort "Erstellen...". Wird dieses ausgewählt, öffnet sich das erforderliche Fenster (in unserem Beispiel also "Parteien"), und der betreffende Eintrag kann vorgenommen werden. So kann man sich Schritt für Schritt durch die Einstellungen hangeln. 

### Vorkonfigurierte Datenbank - Musterdatenbank - verwenden 

Für übliche Standard-Konfigurationen stellen wir Musterdatenbanken und Modullisten zur Verfügung, die die Einrichtung von Tryton erheblich vereinfachen, in diesen bereits angelegt sind die meistens verwendeten Einstellungen. Außerdem enthält sie Mustereinträge, die der Benutzer für sich abändern oder löschen kann:

* Musterpartei Benutzer: "Max Musterman"
* Musterpartei eigenes Unternehmen: "Musterfirma in Musterort"
* Musterpartei Musterkunde: "Testkunde"
* Musterpartei Musterlieferant: "Lieferant GmbH"
* das Geschäftsjahr: vom 1.1. bis 31.12.
* die Währung: Euro
* die Sprache: Deutsch
* die Umsatzsteuer-Sätze: 19% und 7%
* die Zuordnung der USt-Kennziffern (u.a. notwendig für die USt-Voranmeldung)
* Muster-Buchungssatzvorlagen: Porto Barauslage, geringwertige Wirtschaftsgüter, Barauslage, Bankgebühren. Diese können auch als Vorlage für eigene Buchungssatzvorlagen verwendet werden.
* Nummernkreise: Buchungssatzfestschreibung, Kundenrechnung, Lieferantenrechnung, Kunden-Stornorechnung, Lieferanten-Stornorechnung
* Musterpartei: "Musterfirma in Musterstraße, Musterort"
* Musterartikel
* Artikelkategorien - volle MwSt, ermäßigte MwSt
* ein Warenlager mit einem Lagerort

Diese Datenbanken sind verfügbar für die Standard-Kontenrahmen

* SKR03 (Bilanzierung mit Sollversteuerung)
* SKR04 (Bilanzierung mit Sollversteuerung)
* SKR04 (EÜR)
* SKR49 (Stiftungen und Vereine)

Die Wahl ist Geschmackssache, wir verwenden gern den SKR04. Die Konfigurationen können mit geringem Aufwand an die jeweiligen Anforderungen angepasst werden. Wer mit diesen Standard-Konfigurationen nicht zurecht kommt, findet im zweiten Teil des Buchs detaillierte Angaben für die Einrichtung "zu Fuß", die auch zur Anpassung unserer Muster-Datenbanken genutzt werden können.

In unseren Musterdatenbanken sind angelegt:

#### Währung

Im Menüeintrag "Währungen" muß mindestens die eigene Währung eingetragen sein. Mit "Code" ist der [ISO-Code 4217](https://de.wikipedia.org/wiki/ISO_4217) gemeint. Unsere Musterdatenbank enthält eine komplette Liste der Weltwährungen.

#### Parteien

Jede Person oder Körperschaft, die zu dem in Tryton verwalteten Unternehmen in Beziehung tritt, wird als "Partei" bezeichnet. Die betrifft Personen, Unternehmen, Behörden etc. Im eigenen Unternehmen müssen die relevanten Mitarbeiter (v.a. Administratoren und Mitarbeiter, deren Leistungen auf Zeitbasis abgerechnet werden) als eigene Partei angelegt werden. Der Administrator (sofern er Mitarbeiter seiner eigenen Firma ist) muß an drei Stellen in Tryton angegeben werden:

* als "Partei" (![](../wp-content/uploads/icons/tryton-menu.svg) Partei > Partei)
* als "Mitarbeiter" im Unternehmen (![](../wp-content/uploads/icons/tryton-menu.svg) Unternehmen > Mitarbeiter)
* als "Benutzer" (![](../wp-content/uploads/icons/tryton-menu.svg) Verwaltung > Benutzer > Benutzer)

Wir haben "Max Mustermann" in allen drei Kategorien angelegt. Der Anwender kann den Eintrag in "Parteien" für sich abändern, die Änderungen werden in den anderen Kategorien übernommen.

Sinngemäß gilt das gleiche für die Musterfirma, den Musterlieferanten und den Musterkunden.


#### Artikel

Alles, was in Tryton gekauft oder verkauft werden kann, ist ein "Artikel". Darunter fallen auch Dienstleistungen wie Beratungsstunden oder Versand.

Wir haben einen Musterartikel in zwei Varianten sowie einen "Versandartikel" angelegt. Artikel, die in der Buchhaltung erfasst werden sollen, müssen einer Artikelkategorie zugeordnet werden.

#### Artikelkategorien

![](../wp-content/uploads/icons/tryton-menu.svg) Artikel > Kategorien > Kategorien

An dieser Stelle sind Kategorien zu hinterlegen. Davon gibt es in Tryton zwei Varianten:

**"reine" Artikelkategorien**

Dies sind vom Nutzer frei zuordenbar, man kann sie für [Preislisten](#preislisten) und der besseren Strukturierung des Artikelbestandes nutzen. Man kann zum Beispiel

+ einen Artikel mehreren Kategorien zuweisen,
+ Artikelkategorien hierarchisch aufbauen, zum Beispiel "Schrauben" mit den Unterkategorien "Metrische Schrauben", "Schrauben, imperial" oder "Feingewindeschrauben".

**Buchhaltungskategorien**

Wenn der Haken "Buchhaltung" gesetzt ist, kann eine Artikelkategorie auch dienen zur

+ korrekten Verbuchung der Umsatzsteuer
+ Zuordnung eines Artikels zu Aufwands-, Ertrags- und Warenbestandskonten

Diese Zuordnung ist *erforderlich*, damit der An- und Verkauf von Artikeln automatisch korrekt gebucht wird.
In der Musterdatenbank haben wir angelegt:

* Umsätze zum vollen Umsatzsteuersatz (19% USt), Wareneingang und Erlöse
* Versandkosten

Wer auch zum ermäßigten Umsatzsteuersatz verkaufen darf, muß eine weitere entsprechende Artikel-Buchhaltungskategorie anlegen.

#### Journale

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung > Einstellungen > Journale > Journale

Journale fassen Buchungen gleicher Art zusammen und erlauben einen schnelleren Überblick beim Nachvollziehen von Vorgängen. Die Zuweisung von Buchungen zu Journalen erfolgt normalerweise automatisch beim Erzeugen von Einkäufen oder Verkäufen – oder manuell beim Buchen, dort kann Journal von Hand eingetragen werden.

Standardmäßig angelegt sind "EXP" (Aufwand), "REV" (Ertrag), "STO" (Lagerbewegungen) und "CASH" (Zahlungsverkehr). Ggf. ist bei Einzelunternehmen/Personengesellschaften noch ein Journal "PRI" für Privateinlagen/-entnahmen anzudenken oder auch ein Kassenbuch. 

#### Nummernkreise

![](../wp-content/uploads/icons/tryton-menu.svg) Verwaltung > (strikte) Nummerkreise

![Menueintrag "Nummernkreise"](../wp-content/uploads/Tryton_einrichten/Nummernkreise.png)

Hier finden Sie die Liste mit den vorhandenen Nummernkreisen. Ein Klick auf das ![](../wp-content/uploads/icons/tryton-open.svg)-Symbol öffnet die Einstellungen, dort können Sie das gewünschte Zahlenformat anpassen. Tryton unterscheidet "strikte" und "gewöhnliche" Nummernkreise, bei den "strikten" sorgt das System auch bei Mehrbenutzerbetrieb dafür, daß jede Nummer nur einmal vergeben werden kann.

Wir haben Nummernkreise für "Lieferantenrechnung", "Kundenrechnungen", "Lieferanten-Stornorechnungen" und "Kunden-Stornorechnungen" angelegt. Standardmäßig werden die Nummernkreise mit einem neuen Geschäftsjahr zurückgesetzt.

Wir empfehlen dringend die Option feste Länge anzuhaken, dieses fügt führende Nullen an, was zum einen die Sortierung optimiert und zum anderen die lesbarbeit deutlich verbessert, schlicht weil die Dezimalziffern "korrekt" untereinander stehen.

#### Staaten

![](../wp-content/uploads/icons/tryton-menu.svg) Verwaltung > Staaten

Hier ist eine Liste der Staaten hinterlegt, die gegebenenfalls angepaßt werden kann. Unter ![](../wp-content/uploads/icons/tryton-menu.svg) Parteien > Einstellungen > Adressformate können Adressformate der Länder geändert werden.

#### Warenlager

![](../wp-content/uploads/icons/tryton-menu.svg) Lager&Inventur > Lagerorte

Das eigene Warenlager ist gegliedert in

* ein oder mehrere Warenlager. In der Regel handelt es sich dabei um ein Gebäude bzw eine Adresse. Wir haben es "Lagerhaus Musterfirma" genannt, dieses enthält
* den Warenausgangs- und den
* Warenausgangsbereich und
* eine oder mehrere Lagerzonen (Stockwerke, Räume, Regalbereiche, Regalböden etc.), wir haben "Lagerraum 1 Erdgeschoß" und "Lagerraum 2 Keller" angelegt.

Die Namen der Lager lassen sich anpassen. In kleinen Unternehmen läßt sich Lagerzone, Wareneingang und Warenausgang zusammenfassen, siehe [diese Hinweise](https://discuss.tryton.org/t/simplify-warehouse-storage-locations/2840).

#### Preislisten{#preislisten}

*erforderliches Modul: sale_price_list*

![](../wp-content/uploads/icons/tryton-menu.svg) Artikel > Preislisten

Preislisten werden in Tryton sowohl zur Erzeugung von Kundenpreislisten (in Katalogen), als auch für die Abbildung von unterschiedlichen Verkaufspreisen für unterschiedliche Kunden(gruppen) in Kundenrechnungen etc. verwendet. Die Zuordnung von Preislisten zu Kunden geschieht unter ![](../wp-content/uploads/icons/tryton-menu.svg) Parteien > Parteien > Verkauf. Ein klassischer Anwendungsfall sind Preislisten für "Endkunden" und "Wiederverkäufer". Die Funktion ist sehr mächtig, dadurch aber auch komplex in der Anwendung. Für anspruchsvollere Situationen wirken "Preislisten" und "Artikelkategorien" zusammen. 

Artikel, die in keiner Preisliste enthalten sind, werden zum Listenpreis fakturiert. 

##### Anwendung ohne Artikelkategorien

Unter ![](../wp-content/uploads/icons/tryton-menu.svg) Artikel > Preislisten wird mit ![](../wp-content/uploads/icons/tryton-create.svg) eine neue Preisliste angelegt, in der Liste "Positionen" können mit dem kleinen ![](../wp-content/uploads/icons/tryton-create.svg) Einträge hinzugefügt werden. Ein solcher ist 

+ entweder ein Artikel 
+ oder eine Artikelkategorie (s. weiter unten)

Der Eintrag im Feld "Formel" enthält 

+ entweder eine Zahl (den VK-Preis) 
+ oder eine "echte Formel". Verwendet wird Python-Code, hier können Berechnungen mit dem Listenpreis (list_price), dem Einstandspreis (cost_price) oder dem unit_price durchgeführt werden. *Achtung, hier stets den Dezimalpunkt statt eines Kommas verwenden*.

Beispiele:

| Eintrag Formelfeld | Ergebnis |
| --- | --- |
| 4 | Artikelpreis ist 4€ |
| cost_price * 1.5 | Einstandspreis +50% |
| list_price * 0.9 | Listenpreis -10% |

In einer Preisliste können Positionen, die Artikel enthalten, mit solchen, die Artikelkategorien enthalten, kombiniert werden. Wichtig ist, **daß diese Einträge von oben nach unten abgearbeitet werden**. Tryton wendet stets den ersten gefundenen Eintrag an, daher ist die Reihenfolge der Positionen entscheidend. 

##### Anwendung mit Artikelkategorien

Diese werden unter ![](../wp-content/uploads/icons/tryton-menu.svg) Artikel > Kategorien angelegt. In unserem Fall dienen sie dazu, Gruppen von Artikeln zu bilden, auf die die gleiche Kalkulationsregel (oder der gleiche Zahlenwert) angewendet wird. Mit dem Schalter  ( ![](../wp-content/uploads/icons/tryton-add.svg) Artikel hinzufügen ) kann entweder in dem Eingabefeld ein einzelner Artikel eingegeben, oder mehrere Artikel mit ![](../wp-content/uploads/icons/tryton-add.svg)  ausgewählt werden.

Um für einzelne Artikel Ausnahmen für eine Artikelkategorie festzulegen, erzeugt man für diese Artikel Einzeleinträge (oder eine weitere Kategorie), die in der Preisliste **oberhalb** der ersten Artikelkategorie eingeordnet werden.

Sollten Sie Ihre Preise brutto festlegen und auf netto zurückrechnen, ist kann ebenfalls dieses Modul verwendet werden.

## Grundlegende Abläufe

### Warenverkauf tätigen: Vom Angebot zur Rechnung

*Hinweis 1:* Die Abrechnung von Dienstleistungen wird im Absatz [Projektaufwand erfassen und abrechnen](#projektaufwanderfassenundabrechnen) dargestellt.

*Hinweis 2:* Das Vorgehen für die Zuordnung und Abrechnung von Versandkosten werden im Teil 2 des Buchs im  Abschnitt [Versandkosten](https://www.tryton-dach.org/tryton-buch-details/#versandkosten) dargestellt. 

Die am meisten nachgefragte Aktion in einem ERP-System ist "Rechnung schreiben." In Tryton sollte eine Rechnung nicht direkt, sondern als Teil eines Verkaufsvorgangs erzeugt werden. Tryton erzeugt dabei im Hintergrund automatisch die entsprechenden Buchungen in Buchhaltung und Warenwirtschaft, dies betrifft natürlich auch die Umsatzsteuer. Verkäufe werden angelegt unter

![](../wp-content/uploads/icons/tryton-menu.svg) Verkauf > Verkauf

In unserer Musterdatenbank sind alle nötigen Angaben eingetragen, gegebenenfalls kann zum Beispiel ein abweichender Rechnungsempfänger, eine Preisliste o.a. zusätzlich angegeben werden. Die notwendigen Schritte für den Verkauf sind:

#### Angebot erstellen

Das Verkaufsformular wird analog dem Muster ausgefüllt. Unten rechts kann ein Angebot für den Kunden erstellt werden. Mit dem ![](../wp-content/uploads/icons/tryton-open.svg)-Knopf öffnet sich das Angebot in Open/LibreOffice (auf Grundlage des hinterlegten Berichts) und kann dort weiterverarbeitet werden.

#### Bestellbestätigung

Nach Annahme des Angebotes durch den Kunden wird mit dem Knopf "Bestätigen" die Bestellung fertiggstellt, dabei erzeugt Tryton einen Rechnungsentwurf und eine Lieferung (unten links). Mit dem ![](../wp-content/uploads/icons/tryton-open.svg)-Knopf kann eine Bestellbestätigung in Open/LibreOffice geöffnet werden.

#### Lieferung

Damit die bestellten Artikel aus dem Warenbestand ausgebucht werden, muss eine *Lieferung* generiert werden. Dies geschieht über den Menüpunkt unten links in der Bestellung oder über

![](../wp-content/uploads/icons/tryton-menu.svg) Lager&Inventur > Warenausgang

Die Beschriftung der Menüpunkte unten rechts wechseln mit dem Fortgang der Bestellung:

* "Reservieren" blockiert die bestellten Artikel im Lagerbestand,
* "Kommissionieren" ermöglicht die Erzeugung einer Packliste über den ![](../wp-content/uploads/icons/tryton-open.svg)-Schalter
* "Packen" fordert zur Fertigstellung der Sendung auf. Es kann ein Lieferschein erzeugt werden, danach sind keine Änderungen der Bestellung möglich.
* Mit "Erledigt" wird die Buchung festgeschrieben.

#### Rechnung schreiben

Die Rechnung kann erzeugt werden aus der Bestellung oder über

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung > Rechnungen > Kundenrechnungen

* "Prüfen" erzeugt eine ProForma-Rechnung (![](../wp-content/uploads/icons/tryton-open.svg)-Schalter).
* Nach dem "Festschreiben" läßt sich die Rechnung nicht mehr ändern, allenfalls stornieren
* Mit "Zahlung erfassen" lassen sich ein oder mehrere Zahlungseingänge registrieren.

#### Warenrücknahme 

Die Rücknahme einer kompletten Bestellung kann über den betreffenden Verkauf, ![](../wp-content/uploads/icons/tryton-launch.svg)> `Rücknahme erstellen` angestoßen werden. Die betreffende Bestellung wird als neuer Vorgang, alle Artikel mit negativen Stückzahlen erzeugt. Wie bei einem Verkauf können die Artikel wieder eingelagert werden, daraufhin wird eine Kundenrechnung mit negativen Beträgen erzeugt, die wie gewohnt weiterverarbeitet wird.

Die Rücknahme einzelner Artikel oder abweichender Artikellisten werden analog erzeugt - die Artikel aus der Ursprungsbestellung können dabei gelöscht oder angepaßt werden. Wichtig: Immer auf das Minus-Vorzeichen bei den Stückzahlen achten. 

### Wareneinkauf tätigen: Von der Bestellung bis zur Bezahlung

Der beschriebene Einkaufsprozeß ist notwendig, sofern die beschafften Artikel in die Warenwirtschaft aufgenommen werden sollen. Verkauf und Einkauf entsprechen einander weitgehend. Wir stellen daher den Einkauf etwas knapper dar.

![](../wp-content/uploads/icons/tryton-menu.svg) Einkauf > Einkauf

Die Schritte beim Einkauf:

* neuen Einkauf erzeugen
* Artikel hinzufügen
* "Anfrage" erzeugt eine Preisanfrage, kann über den ![](../wp-content/uploads/icons/tryton-open.svg)-Knopf aufgerufen werden
* "Bestellung" erzeugt ein Bestell-Dokument und eine Lieferantenrechnung; letztere ist die Tryton-interne Umsetzung der vom Lieferanten gestellten Rechung.

#### Wareneingangslieferungen erzeugen

Die Wareneingangslieferung wird nicht automatisch erzeugt. Dies geschieht hier:

![](../wp-content/uploads/icons/tryton-menu.svg) Lager&Inventur > Wareneingang

Mit ![](../wp-content/uploads/icons/tryton-create.svg) wird eine neue Wareneingangslieferung erzeugt, es muss der Lieferant eingetragen und auf das richtige Lager geachtet werden. Mit dem ![](../wp-content/uploads/icons/tryton-add.svg)-Knopf wird die Liste bestellter Artikel angezeigt, die ganz oder teilweise übernommen werden kann.

"Einlagern" fügt die Artikel dem Lagerbestand hinzu.

#### Lieferantenrechnung anpassen

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung > Rechnungen > Lieferantenrechnungen

oder im Einkauf unten links. Dieser Entwurf wird gegebenenfalls anhand des Inhalts der erhaltenen Lieferantenrechnung korrigiert und festgeschrieben. Ist die eigene Zahlung erfolgt, kann dies mit "Zahlung erfassen" in Tryton verbucht werden.

### Dienstleistungen / Projektaufwand erfassen und abrechnen{#projektaufwanderfassenundabrechnen}

**Erforderliche Module: `trytond_project` und `trytond_project_invoice`**

*Vorbemerkung*: Unter ![](../wp-content/uploads/icons/tryton-menu.svg) Zeiterfassung kann man normalerweise alles außer dem Menupunkt "Zeiterfassungspositionen" ignorieren, die meisten Einträge werden unter ![](../wp-content/uploads/icons/tryton-menu.svg) Projekte vorgenommen.

Es muß ein "Dienstleistungsartikel" (zum Beispiel "Beratungsstunde telefonisch") mit den Eigenschaften *Dienstleistung, verkäuflich* und der Einheit *'Stunde'* vorhanden sein beziehungsweise angelegt werden.

Unter ![](../wp-content/uploads/icons/tryton-menu.svg) Projekte > Projekt wird ein Projekt angelegt (![](../wp-content/uploads/icons/tryton-create.svg)), unter "Partei" der Kundenname, im Feld "Artikel" der o.e. "Dienstleistungsartikel" eingetragen, unter "Kontaktadresse" kann eine abweichende Kundenadresse hinterlegt werden.

Wenn alle Einträge vorgenommen wurden, unbedingt "speichern" (Strg-S), sonst werden die Abrechnungsmethoden nicht aktiviert. Von diesen existieren folgende:

#### Abrechnungsmethode *manuell*

Tryton speichert hier nur die eingetragenen Zeitaufwände, Rechungen müssen unter *Kundenrechnungen* separat und von Hand angelegt werden. Dies kann in sehr komplexen Zusammenhängen sinnvoll sein.

#### Abrechnungsmethode *nach veranschlagtem Zeitaufwand*

Der veranschlagte Zeitaufwand wird im entsprechenden Feld eingetragen und kann dann direkt abgerechnet werden: Nach *speichern* aller Einträge wird unten rechts die Schaltfläche "Rechnung erstellen" aktiv.

#### Abrechnungsmethode *nach Fortschitt*

Die ist eine Variante der vorher beschriebenen Methode. Statt den gesamten veranschlagten wird nur der im Feld "Fortschitt" eingetragene bereits erbrachte Leistunganteil abgerechnet, dies kann so oft wiederholt werden, bis dort "100%" erreicht werden. Unter dem Reiter "fakturierter Fortschitt" kann dieser beobachtet werden.

#### Abrechnungsmethode *nach erfasstem Aufwand*

Die abzurechnenden Zeitaufwände werden unter ![](../wp-content/uploads/icons/tryton-menu.svg) Zeiterfassung > Zeiterfassungspositionen erfaßt. Unter "Abrechnen bis" kann der Abrechnungsstichtag eingetragen werden.

Die erstellen Rechnungsentwürfe sind unter ![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung > Rechnung > Kundenrechnungen zu finden. Der Reiter "fakturierter Fortschitt" zeigt alle Zeitaufwände an, füŕ die bereits Rechnungsentwürfe erzeugt wurde. Diese können nicht mehr gelöscht werden.

### Skonto buchen

Ein entsprechendes Modul ist im Frühjahr 2022 fast fertig - sobald es verfügbar ist, aktualisieren wir diesen Eintrag. Hier die entsprechende Forumsdiskussion: [](https://discuss.tryton.org/t/early-payments-discounts/2180/9)

Bis es verfügbar ist, kann man einen "Rabatt-Artikel" der entsprechenden Buchhaltungskategorie anlegen und diesen mit dem (negativen) Rabattbetrag als Preis versehen.

### Manuelle Buchung

#### Buchungssätze erstellen

Der Normalfall in Tryton ist die automatische Erstellung von Buchungssätzen im Rahmen eines Einkaufs oder Verkaufs - oder über eine [Buchungssatzvorlage](#buchungssatzvorlagen). Sofern dies nicht möglich ist, werden Buchungssätze manuell erstellt. Dies geschieht unter

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung > Erfassung > Buchungssätze

Mit ![](../wp-content/uploads/icons/tryton-create.svg) wird ein neuer Buchungssatz erzeugt, das kleine ![](../wp-content/uploads/icons/tryton-create.svg) rechts im unteren Bildschirmteil erzeugt einzelne Buchungszeilen. Tryton überprüft mit der Festschreibung die Vollständigkeit und rechnerische Richtigkeit eines Buchungssatzes.

Wer die Funktion "Steuerkennziffern" nutzen will, muß dann noch die umsatzsteuerliche Zuordnung vornehmen. Details dazu sind im nächsten Abschnitt unter [Buchungssatzvorlagen](#buchungssatzvorlagen)" nachzulesen.

#### Buchungssatzvorlagen{#buchungssatzvorlagen}

Buchungssatzvorlagen erleichtern das Buchen wiederkehrender Buchungsposten, wie zum Beispiel "Porto als Auslage", "Bankgebühren", "Treibstoffkosten" etc. Im folgenden wird das Erstellen einer Buchungssatzvorlage anhand eines Tankstellenbelegs beschrieben.

Tankbeleg:
|  |  |
|:--|--:|
|Datum: |06.02.2024|
|Abgabemenge: | 48.12 l|
|Gesamtsumme: | 80,64 €|
| Nettobetrag: | 67,76 €|
|USt. 19% | 12,87 €|

Um eine Vorlage zu erstellen, müssen Sie Administrationsrechte für
den Bereich Rechnungswesen haben. 

Erstellen Sie eine neue Vorlage unter  
![](../wp-content/uploads/icons/tryton-menu.svg) > Rechnungswesen >  Einstellungen > Vorlagen > Buchungssätze  

Geben Sie der Vorlage einen Namen. Dieser wird später in der Liste unter  
![](../wp-content/uploads/icons/tryton-menu.svg) > Rechnungswesen > Erfassung > Buchungssatz von Vorlage  
angezeigt. Weiter wählten Sie das betreffende Journal (z.B. Aufwand) aus.

##### Reiter "Schlüsselwörter"
Unter dem Reiter `Schlüsselwörter` werden mit ![](../wp-content/uploads/icons/tryton-create.svg) Eingabefelder definiert. In den später erzeugten Buchungssätzen werden die Einträge wie folgt verwendet:
* "Name" als Bezeichner für die Variable, die das Programm speichert/verarbeitet (sicherheitshalber möglichst nur Kleinbuchstaben und keine Leerzeichen verwenden !)
* "Zeichenkette" für die Feldbeschriftung
* "Typ" für den Typ der Variablen

In unserem Beispiel Tankquittung können zum Beispiel folgende Schlüsselwörter angelegt werden:

|Name |Zeichenkette | Typ|
|--|--|--|
|kfz_kennz|Kfz-Kennzeichen|Zeichen|
|brutto|Rechnungsbetrag brutto |Numerisch|

Die Variable "brutto" wird im Reiter "Vorlage" weiterverarbeitet. Wird das Feld "erforderlich" angewählt, speichert Tryton den betreffenden Buchungssatz nicht ohne Eingabe dieser Variablen.

Das Belegdatum braucht nicht separat eingegeben werden, Tryton fragt dieses ohnehin unmittelbar nach Aufruf der Buchungssatzvorlage ab.

##### Reiter "Vorlage"
Hier werden mit ![](../wp-content/uploads/icons/tryton-create.svg)  die zu erzeugenden Buchungszeilen angelegt, dies sind mindestens eine Soll- und eine Haben-Zeile, meist kommen noch Zeilen für Umsatzsteuer-Buchungen dazu. In unserem Beispiel (unterstellt ist der SKR04, Barauslage durch einen Gesellschafter):

|Zeile|Konto|Partei|Vorgang|Betrag|
|:--:|--|--|--|--|
|1|6530 Laufende Fahrzeug-Betriebskosten|-|Soll|brutto / 1.19|
|2|1406 - abziehbare Vorsteuer 19%| - |Soll|brutto - brutto / 1.19|
|3|3510 -  Verbindlichkeiten gegenüber Gesellschaftern|leer oder Partei des Gesellschafters|Haben|brutto|

In der Spalte "Betrag" sind hier Formeln eingetragen, die aus dem Brutto-Betrag (Variable 'brutto') Netto und Umsatzsteueranteil ausrechnen.

**Achtung: In Formeln muß stets das Dezimalzeichen '.' verwendet werden, das ',' funktioniert hier nicht !**

Rechnungen mit mehreren USt-Positionen müssen entsprechend mehrere Schlüsselwörter für die Eingabe und mehrere Positionen für die Berechnung enthalten.

##### Umsatzsteuer-Positionen
Mit der bisher getätigten Einrichtung würden die betreffenden Eingaben korrekt auf die hinterlegten Konten gebucht. Es fehlt allerdings noch die Zuordnung zu den für die Umsatzsteuervoranmeldung nötigen Steuertatbeständen.

Diese Zuordnung wird in den die Netto- und die Umsatzsteuerbeträge betreffenden "Positionen"-Zeilen vorgenommen. Dazu die Zeile im Reiter "Vorlage" markieren und das ![](../wp-content/uploads/icons/tryton-open.svg) klicken. Es öffnet sich ein neues Fenster, dort den Reiter "Steuern" auswählen und mit ![](../wp-content/uploads/icons/tryton-create.svg) eine neue Zeile erzeugen:

In unserem Beispiel:

|Zeilentitel|Steuer|Betrag|Typ|
|-|-|-|-|
|Zeile 1, zu Konto 6530:|19% Vorsteuer|brutto / 1.19|Netto|
|Zeile 2, zu Konto 1406:|19% Vorsteuer|brutto - brutto / 1.19|Steuer|
|Zeile 3, zu Konto 3510:|kein Eintrag|||

Da auf das Soll-Konto (hier: 6530) ein Netto-Wert gebucht werden soll, wir aber die Eingabe eines Brutto-Wert vorgesehen haben, muß Tryton den Netto-Wert berechnen, dies geschieht mit der obigen Formel netto = brutto / 1.19.

##### Hauptnachteil
Der größte Nachteil bei Buchungssatzvorlagen ist, daß die USt-Sätze als Zahlenwert und nicht als Variable eingesetzt werden können. Daher müssen Buchungssatzvorlagen bei jeder Mehrwertsteueränderung korrigiert werden.

### Kontoauszüge einlesen

Dazu ist ein zusätzliches Modul notwendig. Für den Umgang mit dem Standard CAMT.053 ist im Sommer 2022 ein Modul in Arbeit, es wird voraussichtlich ab der Tryton-Version 6.6 verfügbar sein. Wir aktualisieren dann diesen Eintrag.

Bereits jetzt verfügbar ist ein Modul für den Standard MT940 von Martin Data Services, Teltow.

## Berichtswesen

### Bilanz{#bilanz}

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Auswertungen / Bilanz

* Menüpunkt öffnen.
* Das aktuelle Datum ist voreingestellt.
* Die Daten werden bis zum eingestellten Datum dargestellt. Für das ganze Jahr muß der Eintrag folglich auf `31.12.[Geschäftsjahr]` geändert werden.
* Die Baumstruktur kann komplett aufgeklappt werden (Umsch-Pfeil rechts - evt. etwas warten!) und zeigt die Beträge der Kontotypen.
* Ein Doppelklick auf eine Bilanz-Zeile öffnet die Liste der zugehörigen Konten und deren Beträge.
* Ein Doppelklick auf ein Kontozeile öffnet die Liste der Buchungen auf diesem Konto.

**Vergleichen**

* Das Geschäftsjahr kann mit einem anderen Geschäftsjahr verglichen werden.
* Das erste Geschäftsjahr wird aus dem Feld Datum verwendet.
* Häkchen 'Vergleichen' aktivieren.
* Ein Datum aus dem zweiten Geschäftsjahr einstellen.
* Es können auch zwei vergangene Geschäftsjahr verglichen werden.
* Es werden jeweils die Buchungen vom Jahresbeginn bis zum eingestellten Datum verglichen.
* ![](../wp-content/uploads/icons/tryton-refresh.svg)-Knopf oder strg-R drücken.

**Export**

Der Export ist über den "Bericht öffnen"-Knopf ![](../wp-content/uploads/icons/tryton-open.svg) verfügbar und wird in Open/LibreOffice geöffnet. Derzeit (v. 6.4) müssen die Zeilen mit Bilanzsumme "0" noch manuell gelöscht werden, eine entsprechende Funktion ist aber in Arbeit.

### Gewinn- und Verlustrechnung

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Auswertungen / Gewinn- und Verlustrechnung (GuV)

* Menüpunkt öffnen.
* Das aktuelle Geschäftsjahr ist voreingestellt.
* Die Baumstruktur kann komplett aufgeklappt werden (Umsch-Pfeil rechts - evt. etwas warten!) und zeigt die Beträge der Kontotypen.
* Ein Doppelklick auf eine GuV-Zeile öffnet die Liste der zugehörigen Konten und deren Beträge.
* Ein Doppelklick auf ein Kontozeile öffnet die Liste der Buchungen auf diesem Konto.

![Die Gewinn- und Verlustrechnung (GuV)](../wp-content/uploads/Steuer_Finanzamt_und_andere_rechtliche_Anforderungen/guvexport.png)


**Zeitraum eingrenzen**

* Die GuV kann auf einen Zeitraum nach Buchungszeitraum und/oder Datum eingegrenzt werden.
* Die Felder von "Buchungszeitraum", "bis Buchungszeitraum" , "Startdatum" , "Enddatum" nach Bedarf ausfüllen.
* Danach den ![](../wp-content/uploads/icons/tryton-refresh.svg)-Knopf oder strg-R drücken (Pfeil im Uhrzeigersinn).

**Vergleichen**

* Es können Buchungszeiträume verglichen werden.
* Der Vergleich kann mit dem Vorjahr erfolgen, es können auch Buchungszeiträume im selben Jahr verglichen werden.
* Häkchen 'Vergleichen' aktivieren.
* Vergleichszeitraum einstellen.
* ![](../wp-content/uploads/icons/tryton-refresh.svg)-Knopf oder strg-R drücken.

![bilanzexport.png](../wp-content/uploads/Steuer_Finanzamt_und_andere_rechtliche_Anforderungen/bilanzexport.png?fileId=23590#mimetype=image%2Fpng&hasPreview=true)

**Export**

Siehe unter [Bilanz.](#bilanz)

### Abschreibungen

siehe [zweiter Teil](https://www.tryton-dach.org/tryton-buch-details/#abschreibungen)

## Steuer, Finanzamt und andere rechtliche Anforderungen

### Umsatzsteuervoranmeldung

Unter

bis v 6.4: ![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Konten und Tabellen / Steuerkennzifferntabelle öffnen

ab v 6.6: ![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Auswertung / Steuerkennzifferntabelle öffnen

werden die relevanten Beträge für die Umsatzsteuervoranmeldung angezeigt, diese können in das entsprechende Elster-Formular übertragen werden.

Von [m-ds](https://m-ds.de) gibt es das nicht-freie Modul mds-account-tax-code-report, das die Einträge dieser Tabelle in einem übersichtlichen Bericht zusammenfaßt.



### Jahresabschluss

#### Abschlussarbeiten

Für die Erstellung des Jahresabschlusses verfügt Tryton über eine Serie von Routinen unter:

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Abschlussarbeiten

Dazu sind die folgenden **fünf Schritte** durchzuführen:

1. Zunächst müssen die **Konten abgestimmt** werden, dabei die Buchhaltung auf offene Posten überprüft. Nicht abgestimmte Buchhaltungskonten werden ins Folgejahr übernommen. Es ist zwar zu empfehlen, aber nicht zwingend zu erforderlich, die Kontenabstimmung komplett durchzuführen.

2. **Abschluß der Journale**

3. **Abschluß der Buchungszeiträume**, dies erfolgt durch Klick auf die entsprechenden Schaltflächen. Diese Buchungszeiträume sind dann nicht mehr bebuchbar.

4. **Erfolgskonten abschließen**. Das bedeutet, dass die Erfolgskonten für das nächste Geschäftsjahr auf Null gesetzt werden, dazu erzeugt Tryton einen Buchungssatz in einem Buchungszeitraum vom Typ Berichtigung (Adjustment) mit der Buchungsnummer 000x.[Jahr].

![Dialog Erfolgskonten abschließen](../wp-content/uploads/Steuer_Finanzamt_und_andere_rechtliche_Anforderungen/dialogabschluss.png)

Als Journal wird "Jahresabschluss" (JAB) verwendet. Als "Buchungszeitraum" kann unter

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Einstellungen / Geschäftsjahre /Buchungszeiträume

ein 13. Buchungszeitraum des abzuschließenden Geschäftsjahres angelegt werden; die Verwendung 12-[fragl. Geschäftsjahr] ist aber auch möglich.

Beim SKR03 können als Habenkonto xxx und als Sollkonto yyy verwendet werden, beim SKR04 als Habenkonto und als Sollkonto 2899.

Es wird dann ein umfangreicher Buchungssatz generiert, der unter

![](../wp-content/uploads/icons/tryton-menu.svg) Buchhaltung / Erfassung /Buchungssätze

einsehbar ist und festgeschrieben werden muß. Damit sind die Erfolgskonten des gewählten Geschäftsjahres für das folgende Geschäftsjahr "genullt".

5. **"Geschäftsjahres abschließen"**. Tryton prüft dabei die Korrektheit der Konten. Wenn keine Korrekturen nötig sind, ist das Geschäftsjahr danach geschlossen. Bei dieser Aktion werden auch die evtl. noch geöffneten Buchungszeiträume geschlossen.

### Bilanz einreichen

Hierfür muß auf externe Dienstleister zurückgegriffen werden, die staatliche Finanzverwaltung stellt keinen entsprechenden Dienst zur Verfügung. Die beiden im folgenden vorgestellten Lösungen sind darauf eingerichtet, die Salden der bebuchten Konten einzulesen. Auf Seiten des Dienstleisters wird die Bilanz neu erzeugt und kann zur Kontrolle mit der in Tryton erstellten Bilanz abgeglichen werden.

#### myebilanz

*[Myebilanz](https://myebilanz.de)* ist ein kostenloses und in der Komfortversion kostenpflichtiges Angebot des Programmieres Matthias Hanft. Es ist für MS Windows verfügbar, der Autor achtet darauf, daß das Programm mittels `Wine` auch stets auf Linux lauffähig ist.

#### ebilanzonline

*[Ebilanzonline](https://ebilanzonline.de)* ist eine kostenpflichtige Dienstleistung des (nicht mehr bundeseigenen) Bundesanzeiger-Verlages.

## Tryton an die eigenen Bedürfnisse anpassen

Tryton ist im höchsten Maße an die eigenen Bedürfnisse anpassbar, praktisch jedes einzelne Element kann verändert werden - entsprechende Programmierkenntnisse vorausgesetzt.

### Favoriten

Oben links neben dem Texteingabefeld ist das Favoriten-Menu ![](../wp-content/uploads/icons/tryton-bookmark.svg). Dort unter "Verwalten" können weitere häufig benutzte Menueintäge ensprechend den hinterlegen Mustern gespeichert werden.

### Tastatur-Kurzbefehle editieren{#tastaturkurzbefehleeditieren}

*Linux*

Zu editieren ist die Datei `~/.config/tryton/[Versionsnummer-des-Klienten]accel.map`. Bei einem Versions-Update bei dem nicht nur die Endziffer der Versionsnummer erhöht wird, muß die Datei ins neue Verzeichnis kopiert werden.

Schritte:

* Klient schließen
* Zeile mit dem entsprechenden Befehl entkommentieren: Semikolon am Zeilenanfang entfernen
* am Zeilenende die gewünschte Tastenkombination eintragen
* speichern, Klient neustarten

*Windows*

*Mac*

## Wartung und Updates

Unsere Skriptsammlung enthält dafür

* `Update-klein-Tryton.sh` - für die Aktualisierung innerhalb einer Versionsnummer: x.y.z => x.y.[letzte-verfügbare]. Hierbei wird die Datenbank nicht verändert, lediglich die Software des Dämon erneuert. Es wird **keine** neue virtuelle Umgebung angelegt.
* `Update-groß-Tryton.sh` - für die Aktualisierung von Hauptversionsnummern: x.y.z => x.y+2.[letzte]. Dabei wird oft die Datenbank verändert, das Skript erzeugt eine neue virtuelle Umgebung. Theoretisch sind größere Aktualisierungsschritte als "y+2" möglich - diese werden allerdings nur erfahrenen Benutzern mit guter Kenntnis der Datenbankstruktur empfohlen.

## FAQ / Tips & Tricks
**Bei Bestellungen wird die Vorsteuer nicht eingerechnet**

Ist im Partei-Datensatz des Lieferanten unter "Buchhaltung" eine Steuerkategorie eingetragen? - Kann normalerweise leer bleiben, dann sollte die Standard-Vorsteuer eingesetzt werden.

**Kann ich mit Tryton eine (TSE)-Ladenkasse betreiben?**

Ja! - Tryton eignet sich bestens als flexibles und leistungsfähiges Hintergrundsystem, Details siehe [https://tryton-dach.org/tse-ladenkasse](https://tryton-dach.org/tse-ladenkasse).

**Beim Einspielen einer Datenbank-Sicherungskopie (sql-dump) wird gemeldet: "unable to drop database because of some auto connections to DB"**

Zunächst die trytond-Prozesse abwürgen: `killall trytond trytond-cron`. Dann auf der Konsole: 
```
$ ps -ef | grep postgres
```
oder
```
$ ps -ef | grep [Name-der-fraglischen-Datenbank]
```

Relevant ist die Zeile, die etwa so aussieht:
```
$ 501 1445 3645 0 12:05AM 0:00.03 postgres: sasha dbname [local] idle
```
Die dritte Zahl (hier 3645) ist die PID des Prozesses, der beendet werden muß.
```
$ sudo kill -9 3645
```
**Wenn ich zum Beispiel in der Bilanz alle Zeilen mit dem Befehl "Umschalt - Pfeiltaste rechts" öffnen will, passiert garnichts.**

Dieser wichtige Befehl hat oft eine Reaktionszeit von mehreren Sekunden! Bitte Geduld.

**Wie lösche/storniere ich eine Rechung richtig?**

Festgeschriebene Rechnungen können aufgrund der Regeln für ordnungsgemäße Buchführung nicht gelöscht werden. Korrekterweise müssen Rechnungen *storniert* werden. Diese Funktion kann in einer geöffneten Kunden- oder Lieferantenrechnung mit ![](../wp-content/uploads/icons/tryton-launch.svg) > stornieren aufgerufen werden. Ist "mit Verrechnung" angeklickt, werden die notwendigen Hintergrundbuchungen automatisch erledigt. Wurde dies vergessen, können diese über "Konten abstimmen" nachgeholt werden.

**Wie buche ich eine Forderung aus ?**

Um eine Forderung "abzuschreiben", wählt man in der betreffenden Rechnung "Zahlung erfassen" und gibt den Betrag "0" ein. Dann öffnet sich ein Dialog, in dem man die Abschreibungsmethode auswählen kann.

**Wie öffne ich ein geschlossenes Geschäftsjahr wieder?**

![](../wp-content/uploads/icons/tryton-menu.svg) > Buchhaltung > Abschlussarbeiten > Geschäftsjahre abschließen 

Standardmäßig werden dort nur die offenen Geschäftsjahre angezeigt, dort also den voreingestellten Filter löschen. Dann werden alle Geschäftsjahre angezeigt und ein Doppelklick auf "wieder öffnen" ist möglich.w

## Die Tryton-Stiftung und Community

Die Stiftung ist nach belgischem Recht verfaßt und hat ihren Sitz in Liége. Ihre Ziele sind 

* die Veranstaltung von Konferenzen, Treffen und Gemeinschaftsaktivitäten
* die Infrastruktur der Seite https://tryton.org zu betreiben,
* die Gemeinschaft der Unterstützer zu organisieren und
* die Marke "Tryton" zu verwalten und zu fördern.

Details unter <https://www.tryton.org/foundation>

Unter https://tryton.community existiert eine kooperierende Plattform, auf der weitere Module, Dokumentation, Werkzeuge etc. unabhängig von https://tryton.org bereitsgestellt werden.

Die Tryton-Gemeinschaft ist mit mehreren hundert Aktiven weltweit tätig, mit Schwerpunkten in Belgien und Südamerika. Der Austausch erfolgt vor allem über das Forum <https://discuss.tryton.org/>.
