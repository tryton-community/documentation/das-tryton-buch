Das Tryton-Buch ist in drei Abschnitte gegliedert:

1. [Grundlagenwissen für den Nutzer](tryton-buch-grundlagen) - die ersten Schritte
2. [Details für den Nutzer](tryton-buch-details) - Hinweise für die Lösung speziellerer Aufgaben
3. [Administrator-Handbuch](tryton-buch-administrator) - Installation und Pflege des Systems

In Kleinunternehmen werden oft alle Rollen von einer Person wahrgenommen, dennoch halten wir diese Unterteilung aus Übersichtlichkeitsgründen für sinnvoll.
