#! /bin/bash

# INSTALLATIONSSKRIPT ZUR NEUINSTALLATION VON TRYTON

# set -x
instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt

killall trytond

sudo apt-get install python3-venv

datum=`date +%d.%m.%Y`
cd ~

echo Benennen Sie das Verzeichnis, in dem die virtuellen Umgebungen für Ihre Tryton-Versionen liegen sollen. Eingabetaste, falls Tryton im Heimatverzeichnis liegen soll.
read tryverz
mkdir ~/$tryverz
pfadtry=~/$tryverz
cd $pfadtry

echo "Möchten Sie die aktuellste Tryton-Version installieren - oder eine andere? (j / Versionsnummer x.y.*)"
read Zielversionsnummer

if [ "$Zielversionsnummer" = "j" ]
then
Zielversionsnummer=
else
Zielversionsnummer=$Zielversionsnummer
fi

echo "Sollen Verzeichnis und Datenbank den gleichen Namen haben (j/n)?"
read gl_Name

if [ "$gl_Name" = "n" ]
then
echo "Wie soll das neue Verzeichnis für die Installation der ersten virtuellen Umgebung für Tryton heißen?"
read neues_Verzeichnis

echo "Wie soll die neue Datenbank heißen?"
read neue_db_Version

else

echo "Wie sollen das neue Verzeichnis und die neue Datenbank heißen?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

fi

mkdir $pfadtry/$neues_Verzeichnis
cd $pfadtry/$neues_Verzeichnis

echo "Bitte geben sie den gewünschten Namen des Datenbank-Benutzers ein:"
read db_Benutzer

sudo apt install postgresql
sudo systemctl start postgresql

python3 -m venv --system-site-packages $pfadtry/$neues_Verzeichnis
cd $pfadtry/$neues_Verzeichnis
. bin/activate
pip install --upgrade pip
pip install install Genshi lxml passlib pycountry forex_python pkg-resources polib psycopg2-binary python-magic python-sql relatorio Werkzeug wrapt
pip install trytond==$Zielversionsnummer proteus==$Zielversionsnummer


echo -e "Sie können jetzt die Auswahl der Module bestimmen.\n\n

Wir haben dazu im Verzeichnis $instskrverz/Modullisten eine Auswahl von Modullisten sowie eine Beschreibung deren Inhalte vorbereitet. Bitte kopieren Sie eine davon ins Verzeichnis $neues_Verzeichnis. Wenn Sie möchten, können Sie Ihre Modulliste nach Ihren Wünschen abändern. Achten Sie bitte darauf, daß die Modulliste stets die Endung '.lst' hat. \n\n

Wenn Sie eine vorkonfigurierte Datenbank verwenden möchten, wählen Sie eine Modulliste, deren Bezeichnung mit der einer Datenbank aus dem Verzeichnis $instskrverz/Datenbanken korrespondiert. Bitte verändern Sie in diesem Falle die Modulliste nicht. (Weiter mit Eingabetaste)"
read

cd $pfadtry/$neues_Verzeichnis
sed -i "s/$/==$Zielversionsnummer/g" *.lst # Zielversionsnummer einfügen'
sed -i '/^trytond/!d' *.lst # 

cd $pfadtry/$neues_Verzeichnis
. bin/activate
pip install -r *.lst

echo -e "\n\n====================================================="
echo "Bitte schreiben Sie sich ein neues Kennwort für Ihren Datenbank-Benutzer $db_Benutzer. (Weiter mit Eingabe)"
read
echo -e "Legen Sie eine neue Datenbank mit dem Namen $neue_db_Version an. Verwenden Sie dazu ein neues Termial und kopieren Sie folgende Befehle ein:\n\n
sudo su - postgres\n
psql\n
CREATE ROLE $db_Benutzer WITH PASSWORD 'Kennwort_des-Datenbank_Benutzers--in-einfachen-Anführungszeichen';\n
ALTER ROLE "$db_Benutzer -- in Anführungszeichen" WITH LOGIN;
\q\n
createdb -O $db_Benutzer $neue_db_Version\n
exit"
echo -e "=====================================================\n\n"
# echo "Als Kennwort verwenden Sie das für Ihren Datenbank-Benutzernamen $db_Benutzer. (Eingabetaste, wenn fertig)"

echo "Haben Sie bereits den *.sql-Export (oder eine Musterdatenbank)einer Tryton-Datenbank, den Sie importieren möchten? (j/n)"
read db_vorh
if [ "$db_vorh" = "j" ]
then
echo "Dann kopieren Sie die Datei bitte in das Verzeichnis $pfadtry/$neues_Verzeichnis und geben ihm die Endung ".sql", falls noch nicht vorhanden. (Eingabetaste, wenn fertig)."
read

psql -U $db_Benutzer -d $neue_db_Version < $pfadtry/$neues_Verzeichnis/*.sql
fi

cp $instskrverz/trytond.conf $pfadtry/$neues_Verzeichnis/

echo "Bitte editieren Sie die Datei $pfadtry/$neues_Verzeichnis/tryton.conf und ersetzen Sie [name-of-database-owner] durch $db_Benutzer (ohne eckige lammern) und [database-password] (ohne eckige lammern) durch das Kennwort des Datenbank-Benutzers. (Eingabetaste, wenn fertig.)"
read

echo -e  "Installieren Sie gegebenenfalls manuell zu installierende Pakete - zum Beispiel von  github oder aus .whl-Dateien - in einem separaten Terminal-Fenster. Die entsprechenden Befehle lauten normalerweise\n\n

$ cd $pfadtry/$neues_Verzeichnis \n
$ . bin/activate
$ pip install [Name-desPakets].whl \n\n

(Eingabetaste, wenn fertig.)"
read
echo Der folgende Schritt kann einige Minuten dauern.
trytond-admin -c trytond.conf -d $neue_db_Version --all -v

echo "Wenn Sie eine weitere Sprache außer Englisch wünschen, geben Sie bitte deren beiden Kennbuchstaben (fr, es, de, it etc.) ein."
read sprache
trytond-admin -c trytond.conf -d $neue_db_Version -l $sprache

echo -e "Starten Sie den trytond-Dämon durch Eingabe des Befehls\n\n 
$ trytond -c trytond.conf -d $neue_db_Version im Verzeichnis $pfadtry/$neues_Verzeichnis. \n\n
in einem neuen Terminal."
 

echo "Laden Sie den zur Ihrer Version $Versionsnummer passenden Tryton-Klienten herunter. Ihr Web-Browser öffnet sich selbsttätig."
read

xdg-open https://downloads.tryton.org

echo Weiter mit Eingabetaste.
read

echo -e "Falls Sie neben Englisch eine weitere Sprache installiert haben, müssen Sie diese in der graphischen Benutzeroberfläche aktivieren:\n
Administration > Localisazion > Languages ==> load translation.\n\n
Außerdem müssen sie die installierten Module aktivieren:\n\n
Administration > Modules > Modules)."
read

echo -e "\nEnde des Skripts - wir wünschen noch einen schönen Tag."

