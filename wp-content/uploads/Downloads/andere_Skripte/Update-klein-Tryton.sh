#! /bin/bash

# UPDATE-SKRIPT TRYTON FÜR UBUNTU-LINUX

killall trytond
set -x

# sicherheitshalber systemweit installiertes Tryton-Zeug entfernen.

datum=`date +%d.%m.%Y`
cd ~

echo "Liegen Ihre virtuellen Tryton-Umgebungen in Ihrem Heimatverzeichnis ? (j/n)"
read heimverz
if [ "$heimverz" = "n" ]
then

echo Geben Sie das Verzeichnis an, in dem derzeit die virtuellen Umgebungen für Ihre Tryton-Versionen liegen.
read tryverz
pfadtry=~/$tryverz
cd $pfadtry

else
pfadtry=~
cd $pfadtry

fi

echo "Wie heißst das Tryton-Verzeichnis?"
read altes_Verzeichnis

: <<KOMMENTARIO
# ab hier Testgelände
neues_Verzeichnis=test_$datum
mkdir $neues_Verzeichnis
cd $neues_Verzeichnis
db_Benutzer=wolf
alte_db_Version=tryton-56-prod
altes_Verzeichnis=tryton-56-prod
neue_db_Version=test_$datum
# hier Ende Testgelände
KOMMENTARIO

cd $pfadtry/$altes_Verzeichnis
. bin/activate
pip freeze --local > $pfadtry/$altes_Verzeichnis/installiert_am_$datum.txt
deactivate

# trap "set +x; sleep 5; set -x" DEBUG

cd $pfadtry/$altes_Verzeichnis
sed '/^trytond/!d' installiert_am_$datum.txt > Liste_zu_inst_$datum.lst # alle Zeilen raus, außer beginnen mit trytond
grep trytond== Liste_zu_inst_$datum.lst > Versionsnummer
sed -i 's/trytond==//' Versionsnummer # -i: Datei wird überschrieben

echo Ihre bisherige Versionsnummer ist; cat Versionsnummer
sed "s/[0-9]$/*/" Versionsnummer > Zielversionsnummer
Zielvers_VAR=$(cat Zielversionsnummer)
echo Ihre neue Versionsnummer ist; echo $Zielvers_VAR
sed -i "s/==.*/==$Zielvers_VAR/" Liste_zu_inst_$datum.lst
echo Sie können jetzt Änderungen an der Datei Liste_zu_inst_$datum.lst im Verzeichnis $pfadtry/$neues_Verzeichnis vornehmen, zum Beispiel Module löschen oder ergänzen.

. bin/activate
pip install -r Liste_zu_inst_$datum.lst
pip install --upgrade pip

# echo Glückwunsch. Sie haben das update nach Tryton $neue_db_Version erfolgreich durchgeführt. Der Tryton-Dämon wurde gestartet. Sie können sich über das Tryton-GUI einloggen.

echo -e "\nEnde des Skripts - wir wünschen noch einen schönen Tag."

