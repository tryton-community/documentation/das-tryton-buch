#! /bin/bash

# UPDATE-SKRIPT TRYTON FÜR UBUNTU-LINUX

killall trytond
# set -x
# sicherheitshalber systemweit installiertes Tryton-Zeug entfernen.

datum=`date +%d.%m.%Y`
cd ~

echo "Liegen Ihre virtuellen Tryton-Umgebungen in Ihrem Heimatverzeichnis ? (j/n)"
read heimverz
if [ "$heimverz" = "n" ]
then

echo Benennen Sie das Verzeichnis, in dem derzeit die virtuellen Umgebungen für Ihre Tryton-Versionen liegen.
read tryverz
pfadtry=~/$tryverz
cd $pfadtry

else
pfadtry=~
cd $pfadtry

fi

# : <<KOMMENTARIO
echo "Haben Verzeichnis und Datenbank den gleichen Namen haben und soll das so bleiben (j/n)?"
read gl_Name

if [ "$gl_Name" = "n" ]
then

echo "Wie heißst das alte Verzeichnis?"
read altes_Verzeichnis

echo "Welche alte Datenbank-Version möchten Sie sichern?"
read alte_db_Version

echo "Wie soll das neue Verzeichnis heißen?"
read neues_Verzeichnis

echo "Wie soll die neue Datenbank heißen?"
read neue_db_Version

else

echo "Wie heißt das alte Verzeichnis und die alte Datenbank?"
read altes_Verzeichnis
alte_db_Version=$altes_Verzeichnis

echo "Wie sollen das neue Verzeichnis und die neue Datenbank heißen?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

fi

mkdir $neues_Verzeichnis

echo "Bitte geben sie den Namen des Datenbank-Benutzers ein:"
read db_Benutzer

: <<KOMMENTARIO
# ab hier Testgelände
neues_Verzeichnis=test_$datum
mkdir $neues_Verzeichnis
cd $neues_Verzeichnis
db_Benutzer=wolf
alte_db_Version=tryton-56-prod
altes_Verzeichnis=tryton-56-prod
neue_db_Version=test_$datum
# hier Ende Testgelände
KOMMENTARIO

cd $pfadtry/$neues_Verzeichnis
pg_dump -U $db_Benutzer -d $alte_db_Version > try_db_Sicherung_$datum.sql


cd $pfadtry/$altes_Verzeichnis
. bin/activate
pip freeze --local > $pfadtry/$neues_Verzeichnis/im_alten_Verz_Installiert_$datum.txt
deactivate

# trap "set +x; sleep 5; set -x" DEBUG

cd $pfadtry/$neues_Verzeichnis
sed '/^trytond/!d' im_alten_Verz_Installiert_$datum.txt > Liste_zu_inst_$datum.lst # alle Zeilen raus, außer beginnen mit trytond
grep trytond== Liste_zu_inst_$datum.lst > Versionsnummer
sed -i 's/trytond==//' Versionsnummer # -i: Datei wird überschrieben

echo Ihre Versionsnummer ist; cat Versionsnummer
echo Bitte geben Sie Ihre Zielversionsnummer im Format "x.y.*" ein:
read Zielversionsnummer
sed -i "s/==.*/==$Zielversionsnummer/" Liste_zu_inst_$datum.lst # Schreibe Zielversionsnummer in Liste_zu_inst_.

echo Sie können jetzt Änderungen an der Datei Liste_zu_inst_$datum.lst im Verzeichnis $pfadtry/$neues_Verzeichnis vornehmen, zum Beispiel Module löschen oder ergänzen.

cp ../$altes_Verzeichnis/trytond.conf .
python3 -m venv --system-site-packages $pfadtry/$neues_Verzeichnis 
cd $pfadtry/$neues_Verzeichnis

/bin/bash -c "$pfadtry/$neues_Verzeichnis"

echo "Installieren Sie gegebenenfalls manuell zu installierende Pakete - zum Beispiel von  github oder aus .whl-Dateien diesem Terminal-Fenster. Schließen Sie dieses wenn Sie damit fertig sind."
read

. bin/activate
pip install --upgrade pip
pip install -r Liste_zu_inst_$datum.lst
pip install psycopg2 Genshi lxml passlib pkg-resources polib psycopg2-binary python-magic python-sql relatorio Werkzeug wrapt

echo -e "\n\n====================================================="
echo -e "Legen Sie eine neue Datenbank mit dem Namen $neue_db_Version an. Verwenden Sie dazu ein neues Termial. Kopieren Sie dazu folgende Befehle ein:\n\nsudo su - postgres\ncreatedb -O $db_Benutzer $neue_db_Version\nexit"
echo -e "=====================================================\n\n"

read
echo Eingabetaste, wenn Sie die neue Datenbank angelegt haben.

echo Als Kennwort geben Sie das für Ihren Datenbank-Benutzernamen $db_Benutzer ein.
psql -U $db_Benutzer -d $neue_db_Version < try_db_Sicherung_$datum.sql

echo "Geben Sie den ersten Teil der SQL-Befehle - vor dem Update - ein. Verwenden Sie dazu ein anderes Termial."
read


trytond-admin -c trytond.conf -d $neue_db_Version --all -v

echo "Geben Sie den zweiten Teil der SQL-Befehle - nach dem Update - ein. Verwenden Sie dazu ein anderes Termial."
read

trytond-admin -c trytond.conf -d $neue_db_Version --all -v

cd $pfadtry/$neues_Verzeichnis
trytond -c trytond.conf -d $neue_db_Version &

echo Glückwunsch. Sie haben das update nach Tryton $neue_db_Version erfolgreich durchgeführt. Der Tryton-Dämon wurde gestartet. Sie können sich über das Tryton-GUI einloggen.

echo -e "\nEnde des Skripts - wir wünschen noch einen schönen Tag."

