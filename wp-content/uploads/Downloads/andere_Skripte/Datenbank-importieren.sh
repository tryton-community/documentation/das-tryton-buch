#! /bin/bash

# INSTALLATIONSSKRIPT ZUR NEUINSTALLATION VON TRYTON

echo Dieses Skript importiert eine vorhandene Datenbank in eine vorhandene Installation mit gleicher Hauptversionsnummer.
read

set -x
instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt

killall trytond

datum=`date +%d.%m.%Y`
cd ~

echo Benennen Sie das Verzeichnis, in dem die virtuelle Umgebung für Ihre Tryton-Version liegt. Eingabetaste für Heimatverzeichnis.
read tryverz
mkdir ~/$tryverz
pfadtry=~/$tryverz
cd $pfadtry

echo "Wie heißt das Verzeichnis der virtuellen Umgebung für Tryton?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

echo "Bitte geben sie den gewünschten Namen des Datenbank-Benutzers ein:"
read db_Benutzer

cd $pfadtry/$neues_Verzeichnis
. bin/activate
pip install --upgrade pip

echo -e "\n\n====================================================="
echo -e "Löschen Sie die Datenbank mit dem Namen $neue_db_Version und legen Sie sie neu an. Verwenden Sie dazu ein neues Termial. Kopieren Sie dazu folgende Befehle ein:\n\n
sudo su - postgres\n
dropdb $neue_db_Version \n
createdb -O $db_Benutzer $neue_db_Version\n
exit"
echo -e "=====================================================\n\n"

echo Eingabe, wenn fertig.
read

echo "Legen Sie die vorhandene Datenbank-Sicherung mit der Endung .sql in das Verzeichnis $pfadtry/$neues_Verzeichnis."
read

psql -U $db_Benutzer -d $neue_db_Version < *.sql

echo Der folgende Schritt kann einige Minuten dauern.
trytond-admin -c trytond.conf -d $neue_db_Version --all -v

echo "Möchten Sie den Tryton-Server jetzt starten? (j/n)"
read starten

if [ "$Zielversionsnummer" = "j" ]
then
trytond -c trytond.conf -d $neue_db_Version
else
fi

echo -e "\nEnde des Skripts - wir wünschen noch einen schönen Tag."

